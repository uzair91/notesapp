//
//  Helper.swift
//  
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import Foundation
import UIKit
import KUIPopupController
import ObjectMapper
import RMMapper


class Helper {
    
}

extension Helper {

//    //user Methods
//    static func getUser() -> User?  {
//
//        if let user = UserDefaults.standard.rm_customObject(forKey: "user") {
//            return DataHandler.sharedInstance.mapUserResponse(response: user as AnyObject)
//        }
//        return nil
//    }
//    static func saveUser(user : AnyObject) {
//
//        UserDefaults.standard.rm_setCustomObject(user, forKey: "user")
//    }
//
//    static func removeUser() {
//        UserDefaults.standard.removeObject(forKey: "user")
//    }
    
    static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    
    static func getDeviceType() -> ScreenType{
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4
        case 1136:
            return .iPhone5
        case 1334:
            return .iPhone6
        case 2208:
            return .iPhone6Plus
        case 2436:
            return .iPhoneX
        default:
            return .Unknown
        }
    }
}
enum ScreenType: String {
    case iPhone4
    case iPhone5
    case iPhone6
    case iPhone6Plus
    case iPhoneX
    case Unknown
}


class FromTopTranslationAnimator: KUIPopupContentViewAnimator {
    func animate(_ parameter: KUIPopupContentViewAnimatorStateParameter, completion: @escaping (Bool) -> Void) {
        let isShow = parameter.isShow
        let containerView = parameter.containerView
        let containerViewY = parameter.containerViewCenterY
        let screenHeight = UIScreen.main.bounds.height
        
        containerViewY.constant = isShow ? -screenHeight : 0.0
        containerView.superview?.layoutIfNeeded()
        
        containerViewY.constant = isShow ? 0.0 : -screenHeight
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.1, options: .allowUserInteraction, animations: {
            containerView.superview?.layoutIfNeeded()
            
            if !isShow {
                containerView.superview?.alpha = 0.0
            }
        }, completion: completion)
    }
}

