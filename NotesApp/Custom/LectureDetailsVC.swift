//
//  LectureDetailsVC.swift
//  NotesApp
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit


protocol RecorderViewDelegate1 : class {
    
    
    
    func didFinishRecording(_ recorderViewController: LectureDetailsVC)
}

class LectureDetailsVC: UIViewController , RecorderDelegate {
   
    @IBAction func deleteLecture(_ sender: Any) {
        
    }
    
    @IBOutlet weak var editBtn: UIButton!
    
    var timer:Timer?
    var timeLeft = 0
    
    var time1 = 0.0
    var timer1: Timer?
    var  ispaused : Bool = false
    var bookmarkCounter = 0
   var  recording: Recording!
    var langcode = ""
    var flagcounter = 0
    
    @IBOutlet weak var BOOKMARKBTN: UIButton!
    var futureTime: TimeInterval = 0.0
    @IBAction func lectureedit(_ sender: Any) {

        
        if(editBtn.imageView?.image == #imageLiteral(resourceName: "save1") )
        {
            
            var refreshAlert = UIAlertController(title: "Aleart", message: "Do you really want to edit this lecture title? ", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
//                self.saveRecordInBookmark()

                
                if(FMDBDatabaseModel.getInstance().updateTitle(descritpion: self.lblLCat.text!, pid: self.pid))
                {
                    SharedHelper().showToast(message: "Update data successfully", controller: self)
//                    LectureDetailsVC.refText1 = self.referencesTextView.text
                    
                    self.lblLCat.isUserInteractionEnabled = false
                    self.lblLCat.becomeFirstResponder()
                    //        editBtn.imageView?.image = #imageLiteral(resourceName: "save1")
                    self.editBtn.setImage(#imageLiteral(resourceName: "ic_mode_edit_24px"), for: .normal)
                    
                    self.navigationController?.popViewController(animated: true)
                    
                    self.dismiss(animated: true, completion: nil)
                    
                }
                else
                {
                    SharedHelper().showToast(message: "Not able to update record ", controller: self)
                }

                print("Handle Ok logic here")
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(refreshAlert, animated: true, completion: nil)
            
        }
        else
        {
            
            lblLCat.isUserInteractionEnabled = true
            lblLCat.becomeFirstResponder()
            //        editBtn.imageView?.image = #imageLiteral(resourceName: "save1")
            editBtn.setImage(#imageLiteral(resourceName: "save1"), for: .normal)
            
        }
        
    
        
        
        
//        lblLCat.become    FirstResponder()
    }
    var strCat : String = ""
      var strTxt : String = ""
        var strTitle : String = ""
        var strDateTime : String = ""
    var duration : String = ""
   
     open weak var delegate: RecorderViewDelegate1?
//    open weak var datasource: RecorderViewDelegate?
   
    var filepath : String = ""
    var filepath1 : String = ""
    var strTime: String = ""
    var image : UIImage?
    @IBAction func saveinbookmark(_ sender: Any) {
        
        
        if(BOOKMARKBTN.imageView?.image == #imageLiteral(resourceName: "bookmarunselected") )
        
        {
            var refreshAlert = UIAlertController(title: "Aleart", message: "Do you really want to save this lecture in bookmark? ", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                self.saveRecordInBookmark()
                print("Handle Ok logic here")
                
                self.viewWillAppear(true)
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
        else
        {
             SharedHelper().showToast(message: "Already in boomark", controller: self)
            
        }
        
       
    
    
    
    }
    
    
    
    @IBAction func deleteRecord(_ sender: Any) {
        
        
        if(self.id==2)
        {
            var refreshAlert = UIAlertController(title: "Aleart", message: "Do you really want to delete lecture?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                self.deleteBookmarkRecord()
                print("Handle Ok logic here")
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
        else
        {
            
            var refreshAlert = UIAlertController(title: "Aleart", message: "Do you really want to delete lecture?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                self.deleteRecord()
                print("Handle Ok logic here")
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(refreshAlert, animated: true, completion: nil)
            
        }
       
        
        
    }
    var lectureModel : LectureModel?
    
    
    var pid : String!
    
     var refText : String = "text conversion is in process"
     static var refText1 : String = ""
    var id : Int = 0
    
    @IBOutlet weak var txtRefrence: UITextView!
    @IBOutlet weak var lbltotalTime: UILabel!
    @IBOutlet weak var lblProgress: UILabel!
    @IBOutlet weak var musicView: IHWaveFormView!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var descriptionView: UIView!
  
    @IBOutlet weak var lblLCat: UITextField!
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lectureheadertitle: UILabel!
    @IBOutlet weak var lebLtime1: UILabel!
    @IBOutlet weak var lbldateTime: UILabel!
    
    @IBOutlet weak var playbtn: UIButton!
    @IBOutlet weak var btnRef: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

 
        
        lblLCat.text = strCat
        lectureheadertitle.text = strCat
        lebLtime1.text = strTime
        lbldateTime.text = strDateTime
//        recording.delegate = delegate as! RecorderDelegate
         image =  #imageLiteral(resourceName: "play-button")
        editBtn.setImage(#imageLiteral(resourceName: "ic_mode_edit_24px"), for: .normal)
        
        
        if(refText == "")
        {
            
            refText = "text conversion is in process"
            self.txtRefrence.text = refText
        
        }
        
        self.txtRefrence.text = refText
        LectureDetailsVC.refText1 = refText
      btnRef.isUserInteractionEnabled = false
         self.BOOKMARKBTN.isUserInteractionEnabled = false
        
        self.musicView.delegate = self
        self.musicView.dataSource = self

        
        
        
        lblLCat.isUserInteractionEnabled = false
        
        print("coming durtion is here\(duration)")
        
         addToolBar(textField: lblLCat)
    
        
        self.time1 = 0.0
        self.progressView.progress = 0.0
         self.lbltotalTime.text = getTime(duration: Double(self.duration) as! TimeInterval)
        
        
      
            getTextByLocal()
        
      

       
        
       bookmarkCounter = FMDBDatabaseModel.getInstance().getBoomarkCount(pid: pid)
        
        if(bookmarkCounter == 1)
        {
            BOOKMARKBTN.setImage(#imageLiteral(resourceName: "Group 79") , for: .normal)
        }
        
       
        print("file path is here\(filepath1)")

        
        
        if (filepath1 != "" )
        {
            if(FMDBDatabaseModel.getInstance().getPID(filepath: filepath1) == "")
            {
                //            user_id:1
                //            audio_id:1
                //            device_type:ios
                //            token:fdgafdgfdh
                
                
                
                
                
                //            let param = [
                //                "token": fcmToken
                //            ]
                
                if(SharedHelper().isInternetAvailable())
                {
                    
                    do {
                        
                        print("urll of recording is here11111 \(RecordLectureVC.recording.url)")
                        
                        let str = RecordLectureVC.recording.url;
                        // let url = URL(fileURLWithPath: str)
                        //                    let audioData = try Data(contentsOf: str, options: NSData.ReadingOptions())
                        
                        var url2 : URL?
                        
                        if let url1 =  URL.init(string: filepath1)
                        {
                            url2 = url1
                        }
                        
                        url2 = SplashVC.path!.appendingPathComponent(url2!.path)
                        print("file new path is here\(url2!)")
                        
                        
                        let audioData = try Data(contentsOf: url2!, options: NSData.ReadingOptions())
                        
                        
                        saveRecordingWheninternetConnection(audioData1: audioData)
                        //            let audioData1 = try NSData(contentsOf: str, options: NSData.ReadingOptions())
                        //            print(audioData)
                        //             print(audioData1)
                        
                        
                        
                        
                        //                    self.sendAudio(audioData1: audioData)
                        
                        
                    } catch {
                        print(error)
                    }
                    
                    
                }
                
                
                
                
                
                
                
                
            }
            else
            {
                
                //            saveRecordingOffline()
                //            SharedHelper().showToast(message: "Check your internet connection", controller: self)
            }
        }
        
        
        
       
        
        
    }
    
    func saveRecordingWheninternetConnection(audioData1 : Data)
    {
        
//        SharedHelper().showSpinner(view: self.view)
        let url = APP_CONSTANT.BASE_URL + APP_CONSTANT.UPLOAD
        
        var fcmToken = ""
        
        if let t =  UserDefaults.standard.string(forKey: "FCMToken") as String?
        {
            fcmToken = t
        }
        
        print("fcmstring\(fcmToken)")
        print(fcmToken)
        
        
        
      print("id is here \(pid)")
        
        if(FMDBDatabaseModel.getInstance().getLanguageCode(pid: pid) == 0)
        {
            print("english here")
            langcode = "en-US"
        }
         if(FMDBDatabaseModel.getInstance().getLanguageCode(pid: pid) == 1)
        {
            print("spanish here")
            langcode = "es-419"
        }
        
        let param = [
            "user_id": "200",
            "audio_id": "200",
            "device_type": "ios",
            "token": fcmToken ,
            "language" : langcode
        ]
        
        
        
        SharedHelper().addAudioFile(url: url, audioData: audioData1, parameters: param)    {
            response in
            print("here is response : \(response)")
            if response.result.value == nil {
                print("No response")
//                SharedHelper().hideSpinner(view: self.view)
                return
            }
            else {
                let responseData = response.result.value as! NSDictionary
//                SharedHelper().hideSpinner(view: self.view)
                let message = responseData["response"] as! String
                let PID = responseData["PID"] as! Int
                
//                pid = String(PID)
                
                
                if(message == "Uploaded Successfully")
                {
                    //                        print("yes yes yes yes yes yes")
                    
//                    SharedHelper().hideSpinner(view: self.view)
//                    SharedHelper().showToast(message: message, controller: self)
                    
                    let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: MainStoryBoard.LectureDetails.rawValue) as! LectureDetailsVC
                    
//                    vc.pid = String(PID)
                    
                   
                    
                    FMDBDatabaseModel.getInstance().updatePID(pid: String(PID), filepath: self.filepath1)
                    
                    
                    print("pid newone is here\(String(PID))")
                    
                    self.pid = String(PID)
                    self.downloadText()
                    
                    //
                    //                        lebLtime1.text = strTime
                    //                        lbldateTime.text = strDateTime
                    
                    
//                    vc.strCat = self.txtTitle.text!
//                    vc.strDateTime = self.txtDate.text!
//
//                    vc.filepath = self.filepath
//
//
//
//                    vc.lectureModel = self.saveRecordDb(pid: String(PID))
//
//                    vc.strTime = (vc.lectureModel?.getlength())! + " seconds"
//                    vc.duration = (vc.lectureModel?.getlength())!
//                    self.txtTitle.text = ""
//                    self.txtCategory.text = ""
//
//
//
//                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    
                    
                    
                    //                    let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: MainStoryBoard.References.rawValue) as! LectureDetailsVC
                    //
                    //                    self.navigationController?.pushViewController(vc, animated: true)
                    //
                    
                    
                }
                
                
            }
        }
    }
    
    func getTextByLocal()
    {
        
        
        
        let text = FMDBDatabaseModel.getInstance().getDescription(pid: pid)
        
        print("converted text is here \(text)")
        
        
        if(text == "" )
        {
            print("here is aaa")
            downloadText()
        }
        
        
        
        if(text == "text conversion is in process")
        {
            downloadText()
        }
        else
        {
            
            self.btnRef.isUserInteractionEnabled = true
            self.BOOKMARKBTN.isUserInteractionEnabled = true
            self.txtRefrence.text = text
        }
        
        
      
        
        
        
    }
    
    func downloadText() {
        
        
        // in backgrounf fetching text by voice
        
        DispatchQueue.global(qos: .background).async {
            print("In background")
         
            
            self.getTextById()
              
                    DispatchQueue.main.async {
                        print("dispatched to main")
                       
                    }
 
        }
    }
    
     func prepareTimer()
     {
        
        
        SharedHelper().showSpinner(view: self.view)
        timer1 = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(setProgress), userInfo: nil, repeats: true)

     }
    
 
    
    @objc func timepause()
    {
        timer1?.invalidate()
//        ispaused = true
    
    }
    
    
    @objc func setProgress() {
       
//         SharedHelper().showSpinner(view: self.view)
        
        
        let duration = Double(self.duration)
        
        
        if(time1<=0.0)
        {
            time1 = 0.0
        }
        if time1 >= duration as! TimeInterval {

             image = #imageLiteral(resourceName: "update-arrows")
            playbtn.setImage(#imageLiteral(resourceName: "update-arrows"), for: .normal)
//            musicView.player.play()
            
        }
        else
        {
            
            
            if(ispaused==false)
            {
                time1 += 1.0
  
                
               
                self.lblProgress.text = getTime(duration: time1)
                
                
              
                 SharedHelper().hideSpinner(view: self.view)
                
                self.progressView.setProgress(Float(self.time1/duration!), animated: true)
   
                
            }
            else
            {
                timer1!.invalidate()
                
                
                
            }
        }
       
        
    }

    
    override func viewDidDisappear(_ animated: Bool) {
        
        if(musicView.player.isPlaying)
        {
             musicView.player.stop()
        }

    }
    
    
    func pauseTimer()
    {
        //Get the difference in seconds between now and the future fire date
//        timeUntilFire =
       timer1 = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(timepause), userInfo: nil, repeats: true)
        
//        timer1?.invalidate()
    }
    
    func resumeTimer()
    {
        //Start the timer again with the previously invalidated timers time left with timeUntilFire
         timer1 = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(setProgress), userInfo: nil, repeats: true)
    }
    
    
    
    
    //to get refrence
    
    //to play audio
    @IBAction func btnPlay(_ sender: Any) {
    
        
        if(image == #imageLiteral(resourceName: "ic_pause_24px"))
        {
            
            resumeTimer()
            
            
            if(musicView.player.isPlaying)
            {
                musicView.player.stop()
            }
         
             image = #imageLiteral(resourceName: "play-button")

             playbtn.setImage(image, for: .normal)
            

            
             print("duration111111\(musicView.player.duration)")
             print("duration\(getTime(duration: musicView.player.duration))")
            
 
            timeLeft = Int(round(musicView.player.duration))
            
            print("time left is here \(timeLeft)")
            
             ispaused = true
           

        }
            
    
       else if(image == #imageLiteral(resourceName: "update-arrows"))
        {
//            timer!.invalidate()
            image = #imageLiteral(resourceName: "ic_pause_24px")
            //             print("duration\(musicView.player.duration)")
            //
            //            print("current time\(musicView.player.currentTime)")
            playbtn.setImage(image, for: .normal)
            musicView.player.play()
            timer1?.invalidate()
            
            self.time1 = 0.0
            self.progressView.progress = 0.0
            self.lbltotalTime.text = getTime(duration: Double(self.duration) as! TimeInterval)
            
            
            self.prepareTimer()
           
            print("uzair here hahahaa")
        }
       else
        {
        
//            ispaused=true
           
//            setProgress()
            
//            timer1?.invalidate()
            
            
            if(flagcounter==0)
            {
                
                 prepareTimer()
                
            }
            
            flagcounter = flagcounter + 1
            
            
            
            
           
            
            ispaused = false
            
            musicView.player.play()
           
            image = #imageLiteral(resourceName: "ic_pause_24px")
            playbtn.setImage(image, for: .normal)
            
            
        }
    
        
    }

    func update(timer: Timer)
    {
        
   
        
        let timeRemaining = futureTime - NSDate.timeIntervalSinceReferenceDate
        if timeRemaining > 0.0
        {
//            label.text = String(format: "%.07f", timeRemaining)
            print("time remaingin is here \(timeRemaining)")
        }
        else
        {
            timer.invalidate()
            //Force the label to 0.0000000 at the end
//            label.text = String(format: "%.07f", 0.0)
        }
        
      
    }
    
    func getTime(duration : TimeInterval)->String
    {
        let duration = Int((duration))
        let minutes2 = duration/60
        let seconds2 = duration - minutes2 * 60
        let string11 = NSString(format: "%02d:%02d", minutes2,seconds2) as String
        
        return string11
        
    }
    
    //to forward
    @IBAction func btnForward(_ sender: Any) {
        let duration1 = Double(self.duration)
        
        print("duration1 \(getTime(duration: duration1!))")
        
        if(self.lblProgress.text! < getTime(duration: duration1!))
        {
            var currentTime = musicView.player.currentTime
            
            
            if(musicView.player.isPlaying)
            {
                musicView.player.stop()
            }
           
            currentTime = currentTime + 10.0
            musicView.player.play(atTime: currentTime) // plus 30 seconds
            
            musicView.player.currentTime+=10.0
            time1 += 10.0
            if(Double(currentTime) >= duration1!)
            {
                
                self.progressView.setProgress(Float(duration1!/duration1!), animated: true)
                self.lblProgress.text = getTime(duration: duration1!)
                print("current time is \(currentTime)")
                
            }
            else
            {

                self.progressView.setProgress(Float(self.time1/duration1!), animated: true)
                self.lblProgress.text = getTime(duration: time1)
                
                print("current time is \(currentTime)")
            
            }
            
        }
 
    }
    
    //to reverse
    @IBAction func btnReverse(_ sender: Any) {
        
         let duration = Double(self.duration)
        if(self.lblProgress.text != getTime(duration: duration!))
        {
        
            var currentTime = time1
            
            
            if(musicView.player.isPlaying)
            {
                musicView.player.stop()
            }
           
            currentTime = currentTime - 10.0
            musicView.player.play(atTime: currentTime) // plus 30 seconds
            
            musicView.player.currentTime-=10.0
            time1 -= 10.0
            if(currentTime<=0.0)
            {
                
                self.progressView.setProgress(Float(0.0/duration!), animated: true)
                self.lblProgress.text = getTime(duration: 0.0)
                print("current time is \(currentTime)")
                
            }
            else
            {
                
                //        self.progressView.setProgress(Float(time1/duration), animated: true)
                
                
                self.progressView.setProgress(Float(self.time1/duration!), animated: true)
                self.lblProgress.text = getTime(duration: time1)
                
                print("current time is \(currentTime)")
            }
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
       
            super.viewWillAppear(true)
            

        
        self.txtRefrence.text = LectureDetailsVC.refText1
        bookmarkCounter = FMDBDatabaseModel.getInstance().getBoomarkCount(pid: pid)
        
        if(bookmarkCounter == 1)
        {
            BOOKMARKBTN.setImage(#imageLiteral(resourceName: "Group 79") , for: .normal)
        }

    }
    
    @IBAction func editbtn(_ sender: Any) {
    }
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func getReferencesBtnTapped(_ sender: Any) {
//        let vc =
        
        let vc = UIStoryboard.categoryStoryboard().instantiateViewController(withIdentifier: CategoryStoryBoards.PremiumPlan.rawValue) as! PremiumPlanVC
        self.navigationController?.pushViewController(vc, animated: true)
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func referencesBtnTapped(_ sender: Any) {
        let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: MainStoryBoard.References.rawValue) as! ReferencesVC
        
        
        
        vc.allText = txtRefrence.text;
        vc.strDateTime = strDateTime
        vc.strTitle = strTitle
        vc.strCat = strCat
        vc.pid = pid
        vc.title1 = strCat
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func deleteRecord()
    {
        
        print("deleted pid is here \(lectureModel!.processID)")
        
        if(FMDBDatabaseModel.getInstance().deleteRecordFromRecording(pid: lectureModel!.processID))
        {
            
            SharedHelper().showToast(message: "Delete Data successfully", controller: self)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                 _ = self.navigationController?.popViewController(animated: true)
            })
       
            
        }
        else
        {
            SharedHelper().showToast(message: "Not able to delete ", controller: self)
        }
    }
    
    func deleteBookmarkRecord()
    {
        print("deleted pid is here \(lectureModel!.processID)")
        
        if(FMDBDatabaseModel.getInstance().deleteRecordFromBookmark(pid: lectureModel!.processID ))
        {
            
           
            
            SharedHelper().showToast(message: "Delete Data successfully", controller: self)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                _ = self.navigationController?.popViewController(animated: true)
            })
            
            
            
            
            
        }
        else
        {
            SharedHelper().showToast(message: "Not able to delete ", controller: self)
        }
        
    }
    
    func saveRecordInBookmark()
    {
        

        if(FMDBDatabaseModel.getInstance().InsertDatainBookmark(lectureModel!))
        {
            
                SharedHelper().showToast(message: "Save in bookmark successfully", controller: self)
                
            
           
            
        }
        else
        {
            SharedHelper().showToast(message: "Not able to save in bookmark ", controller: self)
        }
        
    
        //        recording.stop()
        //        FMDBDatabaseModel.getInstance().getData()
        
    }
}

extension LectureDetailsVC: IHWaveFormViewDelegate {
    func didFinishPlayBack() {
        print ("playBack Finished")
        
        
    }
    
    func didStartPlayingWithSuccess() {
        
      
        print ("Playback started successfully")
        
    }
    
    
    
    
  
    
    
    
    func getTextById()
    {
//        SharedHelper().showSpinner(view: self.view)
      
        
        let url = APP_CONSTANT.BASE_URL + APP_CONSTANT.FETCH
        
//        print("p_id is here \(pid!)")
        
        let param = [
            "PID": pid!
            
        ]
//        self.lbltotalTime.text = getTime(duration: musicView.player.duration)
        
       
            
            if(self.txtRefrence.text == refText)
            {
                SharedHelper().PostRequest(url: url,  parameters: param)    {
                    response in
                    print(response)
                    if response.result.value == nil {
                        print("No response")
                        //                SharedHelper().hideSpinner(view: self.view)
                        return
                    }
                        
                        
                    else {
                        let responseData = response.result.value as! NSDictionary
                        let text = responseData["response"] as! String
                        
                        
                         print("text is here \(text)")
                        
                        self.txtRefrence.text = text
                        self.btnRef.isUserInteractionEnabled = true
                        FMDBDatabaseModel.getInstance().updateDescription(descritpion: text, pid :self.pid)
                        
                        self.lectureModel?.setdecsricption(decsricption: text)
                        
                        self.BOOKMARKBTN.isUserInteractionEnabled = true
                        
                        
                        //                SharedHelper().hideSpinner(view: self.view)
                        // print(responseData)
                    }
                }
            }
            else{
                 self.BOOKMARKBTN.isUserInteractionEnabled = true
            }
        
        if(self.id==2)
        {
           
            
            self.btnRef.isUserInteractionEnabled = true
            
        }
        else{
            self.btnRef.isUserInteractionEnabled = true
            self.BOOKMARKBTN.isUserInteractionEnabled = true
           
        }
        

        
        
    }
}

extension LectureDetailsVC: IHWaveFormViewDataSource {
    
    
   
    
    func urlToPlay() -> URL {
        var url : URL?
        
        
        
        

        if let url1 =  URL.init(string: filepath)
        {
            url = url1
        }
        else{
            url = SplashVC.path!.appendingPathComponent(filepath1)
            
        }
        
        do{
            recording = Recording(to:url!.path)
            
//            print("url new one is hereaaa\()")

            
            recording.delegate = self
            try recording.prepare()
        } catch let error {
            print("Error: \(error)")
        }
        
        print("path is here \(url!)")
        
        
        ////////// NOTE /////////
        
        // fir stop voice for the first time i changed the ihwaveform class and comment the play in getPath() function
       
        /////////////////////////

        
        return url!
    
    }
    
    func shouldPreRender() -> Bool {
        
        
       
        return true
    }
    
}
