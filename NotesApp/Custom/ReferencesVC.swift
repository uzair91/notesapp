//
//  ReferencesVC.swift
//  NotesApp
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit

class ReferencesVC: UIViewController {

    @IBOutlet weak var referenceHeader: NSLayoutConstraint!
    @IBOutlet weak var bottomContraint: NSLayoutConstraint!
    @IBOutlet weak var referencesTextView: UITextView!
       var strTxt : String = ""
    var strTitle : String!
    var strDateTime : String!
     var strCat : String!
    
    var pid : String = ""
    var title1 : String = ""
    
    @IBOutlet weak var full: UIView!
    @IBOutlet weak var lblTitle: UILabel!
     @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var txtEdit: UITextField!
     @IBOutlet weak var editView: UIView!
    var allText : String!
    @IBOutlet weak var editbtn: UIButton!
    @IBAction func editBtn(_ sender: Any) {

        
        
        if(editbtn.imageView?.image == #imageLiteral(resourceName: "save") )
        {
            var refreshAlert = UIAlertController(title: "Aleart", message: "Do you really want to update this description? ", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                //            self.saveRecordInBookmark()
                print("Handle Ok logic here")
                
                if(FMDBDatabaseModel.getInstance().updateDescription(descritpion: self.referencesTextView.text, pid: self.pid))
                {
                    SharedHelper().showToast(message: "Update data successfully", controller: self)
//                    self.editbtn.imageView?.image = #imageLiteral(resourceName: "ic_mode_edit_24px-1")
                    self.editbtn.setImage(#imageLiteral(resourceName: "ic_mode_edit_24px-1"), for: .normal)
                    LectureDetailsVC.refText1 = self.referencesTextView.text
                    self.referencesTextView.isEditable = false
                }
                else
                {
                    SharedHelper().showToast(message: "Not able to update record ", controller: self)
                }
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
                 self.referencesTextView.isEditable = false
            }))
            
            present(refreshAlert, animated: true, completion: nil)
            
        }
        else
        {
             editbtn.setImage(#imageLiteral(resourceName: "save"), for: .normal)
            
            referencesTextView.isEditable = true
            referencesTextView.becomeFirstResponder()
            referencesTextView.isUserInteractionEnabled = true
        }
        
       
        
       
        
        
//        navigationController?.viewControllers.forEach { ($0 as? LectureDetailsVC)?.reloadInputViews() }
//
//        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var header: UIView!
    //    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
//    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblDate: UILabel!
//    @IBOutlet weak var btnEdit: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        referencesTextView.delegate = self
        
        referencesTextView.isEditable = false
//        lblTitle.text = strTitle
//        lblDate.text = strDateTime
//        lblCategory.text = strCat
//        mainViewHeight.constant = 43
//        viewHeight.constant = 0
        referencesTextView.text = allText
       
        addToolBar1(textField: referencesTextView)
//        self.automaticallyAdjustsScrollViewInsets = true
        
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//        setupViewResizerOnKeyboardShown()
        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(self.keyboardNotification(notification:)),
//                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
//                                               object: nil)
//        self.bindToKeyboard()
        
//        referencesTextView.unbindToKeyboard()
//        editView.isHidden = true
//
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
       
      
        
        self.lblTitle.text = title1
       
        // Do any additional setup after loading the view.
        referencesTextView.layer.borderWidth = 1.0
        referencesTextView.layer.borderColor = UIColor.lightGray.cgColor
        
        
//        if let textRange = referencesTextView.selectedTextRange {
//
//            strTxt = referencesTextView.text(in: textRange)
//
//        }
        
    }
   
    
//    @objc func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if view.frame.origin.y == 0 {
//                self.view.frame.origin.y -= full.frame.height
//            }
//        }
//    }
//
//    @objc func keyboardWillHide(notification: NSNotification) {
//        if view.frame.origin.y != 0 {
//            self.view.frame.origin.y = 0
//        }
//    }
    
//    deinit {
////        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
////
////          NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
//    }
    
    
    
//    @objc func keyboardWillChange(notification: NSNotification) {
//
//        guard let keyboardRect = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else
//        {
//            return
//        }
//
//        if(notification.name == Notification.Name.UIKeyboardWillShow || notification.name == Notification.Name.UIKeyboardWillChangeFrame)
//
//        {
//
//             self.view.frame.origin.y = keyboardRect.height
//        }
//        else
//        {
//            self.view.frame.origin.y = 0
//        }
//
//
//    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame!.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.bottomContraint?.constant = 0.0
            } else {
                self.bottomContraint?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }

//    @objc func keyboardWillShow1(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if view.frame.origin.y == 0 {
//                self.view.frame.origin.y -= keyboardSize.height
//            }
//        }
//    }
//
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("yes yes")
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        print("changing changing ...")
        
        return true
    }
    
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if let textRange = textView.selectedTextRange {
            
            let selectedText = textView.text(in: textRange)
            strTxt = selectedText!
            
            print("text range is \(selectedText)")
            

//            btnEdit.isHidden = false
            
        }
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        var info = notification.userInfo
        let keyBoardSize = info![UIKeyboardFrameEndUserInfoKey] as! CGRect
        referencesTextView.contentInset = UIEdgeInsetsMake(0.0, 0.0, keyBoardSize.height, 0.0)
        referencesTextView.scrollIndicatorInsets = UIEdgeInsetsMake(0.0, 0.0, keyBoardSize.height, 0.0)
    }
    
    @objc func keyboardDidHide(notification: NSNotification) {
        
        referencesTextView.contentInset = UIEdgeInsets.zero
        referencesTextView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func editTap(_ sender: Any) {
        
//        if(!strTxt.isEmpty)
//        {
//
//
//
////        mainViewHeight.constant = 91
////        viewHeight.constant = 48
////
////        editView.isHidden = false
//
//       // txtEdit.text = strTxt
//
//        }
    }
    
   
    @IBAction func btnRefrence(_ sender: Any) {
        
//        if(!strTxt.isEmpty)
//        {
//            let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: "ReferencesDetailVC") as! ReferencesDetailVC
//
//            vc.strTitle = strTxt
//
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        }
        
        let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: "ReferencesDetailVC") as! ReferencesDetailVC
        
        print("start text is here \(strTxt)")
        
        
       
        
        if let textRange = referencesTextView.selectedTextRange {
            
            let selectedText = referencesTextView.text(in: textRange)
            strTxt = selectedText!
//             SharedHelper().showToast(message: strTxt, controller: self)
            
            print("text range is \(selectedText)")
            
            
            //            btnEdit.isHidden = false
            
        }
//        else
//
//        {
//             SharedHelper().showToast(message: "error", controller: self)
//        }
        
        
        if(strTxt == "")
        {

            
            SharedHelper().showToast(message: "Please select text for references", controller: self)
        }
        else
        {
            vc.strTitle = strTxt
            vc.pid = pid
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        
    }
    
    
    @IBAction func btnDoneEdit(_ sender: Any) {
        
      
        
    
        
      // let txt =  referencesTextView.replace(referencesTextView.selectedTextRange!, withText: txtEdit.text!)
      //  print(txt)
//            mainViewHeight.constant = 43
//            viewHeight.constant = 0
            referencesTextView.text = allText
//            editView.isHidden = true
        
     //   }
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension UITextField{
    
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}

extension UIViewController: UITextFieldDelegate{
    func addToolBar(textField: UITextField){
        var toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        var doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: "donePressed")
        var cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: "cancelPressed")
        var spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
       
            
            textField.delegate = self
            textField.inputAccessoryView = toolBar
        
        
    }
    @objc func donePressed(){
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "donePressed"), object: nil)

        
       
        
        view.endEditing(true)
        
        
    }
    @objc func cancelPressed(){
        view.endEditing(true) // or do something
    }
}

extension UIViewController: UITextViewDelegate{
    func addToolBar1(textField: UITextView){
        var toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        var doneButton = UIBarButtonItem(title: "Done", style:
            UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        var cancelButton = UIBarButtonItem(title: "Cancel", style:
            
           
            UIBarButtonItemStyle.plain, target: self, action:#selector(cancelPressed))
        var spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        
        print("isfocues\(textField.isFocused)")
        print("isfocues1\(textField.isSelectable)")
      
       
            
            textField.delegate = self
            textField.inputAccessoryView = toolBar
        
       
    }
    func donePressed1(){
        view.endEditing(true)
    }
    func cancelPressed1(){
        view.endEditing(true) // or do something
    }
}






//extension ReferencesVC {
//    func setupViewResizerOnKeyboardShown() {
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(ReferencesVC.keyboardWillShowForResizing),
//                                               name: Notification.Name.UIKeyboardWillShow,
//                                               object: nil)
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(ReferencesVC.keyboardWillHideForResizing),
//                                               name: Notification.Name.UIKeyboardWillHide,
//                                               object: nil)
//    }
//    @objc func keyboardWillShowForResizing(notification: Notification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
//            let window = self.view.window?.frame {
//            // We're not just minusing the kb height from the view height because
//            // the view could already have been resized for the keyboard before
//            self.view.frame = CGRect(x: self.view.frame.origin.x,
//                                     y: self.view.frame.origin.y,
//                                     width: self.view.frame.width,
//                                     height: window.origin.y + window.height - keyboardSize.height)
//        } else {
//            debugPrint("We're showing the keyboard and either the keyboard size or window is nil: panic widely.")
//        }
//    }
//    @objc func keyboardWillHideForResizing(notification: Notification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
//            let viewHeight = self.view.frame.height
//            self.view.frame = CGRect(x: self.view.frame.origin.x,
//                                     y: self.view.frame.origin.y,
//                                     width: self.view.frame.width,
//                                     height: viewHeight + keyboardSize.height)
//        } else {
//            debugPrint("We're about to hide the keyboard and the keyboard size is nil. Now is the rapture.")
//        }
//    }
//}

