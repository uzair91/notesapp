//
//  UIColor+Extension.swift
//  
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit


extension String {
    var convertIntoFloat: Float {
        return (self as NSString).floatValue
    }
}
