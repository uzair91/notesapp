//
//  UIColor+Extension.swift
//  
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit

extension UIColor {
    //*047616 navigation top darkGreen color*//
    //*05b421 navigation top lightGreenShade color*//
    //*098c1e navigation top green color*//
   
    static func appColorFromRGB(red:CGFloat,green:CGFloat ,blue:CGFloat ) -> UIColor{
      return UIColor.init(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1.0)
    }
    static func appColorDarkGreen() -> UIColor {
        //DarkGreen
        return hexStringToUIColor(hex: "#047616")
    }
    static func appColorlightGreen() -> UIColor {
        //lightGreenShade
        return hexStringToUIColor(hex: "#05b421")
    }
    static func appColorGreen() -> UIColor {
        //Green
        return hexStringToUIColor(hex: "#098c1e")
    }
    static func appBlueColor() -> UIColor{
        return UIColor.init(red: 13.0/255.0, green: 97.0/255.0, blue: 163.0/255.0, alpha: 1.0)
    }
    
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}
