//
//  UIFont+Extention.swift
//  
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit

extension UIFont {
    
    
    /*
     Open Sans
     == OpenSans-Bold
     == OpenSans-Light
     == OpenSans-Semibold
     == OpenSans
     for family: String in UIFont.familyNames
     {
     print("\(family)")
     for names: String in UIFont.fontNames(forFamilyName: family)
     {
     print("== \(names)")
     }
     }
     */
    
    static func appFontRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans", size: size)!
    }
    static func appFontBold(size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Bold", size: size)!
    }
    static func appFontLight(size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Light", size: size)!
    }
    static func appFontSemiBold(size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Semibold", size: size)!
    }
   static func appSystemFonts(size:CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size)
    }
}
