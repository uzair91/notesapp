//
//  UIStoryBoard+Extension.swift
//
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit

enum MainStoryBoard : String {
    
    case Main = "MainVC",
    LeftMenu = "LeftMenuVC",
    Splash = "SplashVC",
    RecordLecture = "RecordLectureVC",
    LectureDetails = "LectureDetailsVC",
    References = "ReferencesVC",
    ReferencesDetail = "ReferencesDetailVC",
    ReferenceFullTextController = "ReferenceFullTextController"
    
}

enum LecturesStoryBoard : String {
    
    case Lectures = "LecturesVC",
    SearchLecture = "SearchLecturesVC",
    BookmarkLectures = "BookmarkLecturesVC"
    
}

enum CategoryStoryBoards : String {
    case BrowseCategory = "BrowseCategoryVC",
    CategoryName = "CategoryNameVC",
    NewLectureCategory = "NewLectureCategoryVC",
    PremiumPlan = "PremiumPlanVC"
}

enum SettingsStoryBoards : String {
    case About = "About_Us",
    Feedback = "FeedbackVC",
    Notifications = "NotificationsVC"
}

extension UIStoryboard {
    static func mainStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    static func lecturesStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Lectures", bundle: nil)
    }
    static func categoryStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Category", bundle: nil)
    }
    static func settingsStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Settings", bundle: nil)
    }
}

