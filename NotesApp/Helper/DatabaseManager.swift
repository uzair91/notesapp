//
//  DatabaseManager.swift
//  NotesApp
//
//  Created by Munzareen Atique on 3/11/19.
//  Copyright © 2019 APP. All rights reserved.
//

import UIKit
import FMDB
import SQLite3
class DatabaseManager {
    
    private let dbFileName = "database.db"
    private var database:FMDatabase!
    
    
    public var  TABLE_NAME = "saved_recordings";
    
    public var  COLUMN_NAME_LECTURE_TITLE = "lecture_title";
    public var  COLUMN_NAME_LECTURE_CATEGORY = "lecture_category";
    public var COLUMN_NAME_LECTURE_DATE = "lecture_date";
    public var COLUMN_NAME_LECTURE_DESC = "lecture_desc";
    public var COLUMN_NAME_LECTURE_BOOKMARK = "lecture_bookmark";
    public var COLUMN_NAME_RECORDING_NAME = "recording_name";
    public var COLUMN_NAME_RECORDING_FILE_PATH = "file_path";
    public var COLUMN_NAME_RECORDING_LENGTH = "length";
    public var COLUMN_NAME_TIME_START = "time_start";
    public var COLUMN_NAME_TIME_END = "time_end";
    public var COLUMN_NAME_PROCESS_ID = "pid";
    public var COLUMN_NAME_IS_CONVERTED = "is_converted";
    public var COLUMN_NAME_IS_DOWNLOADED = "is_downloaded";
    public var COLUMN_NAME_LANGUAGE = "lang";
    
     public var CATEGORYNAME = "cat_name";
    public var _ID : Int?
    
    private var fileURL : URL?
     private var fileURL1 : URL?
    var db: OpaquePointer?
    
    init() {
       
        opendb()
       
//        addRecording(title: "title1", category: "lecture1", recordingName: "recording1", filePath: "u:c", length: 2.0, timeStart: 2.0, processID: "56")
        
//        deleteAll()
        
//        saveData()
    }
    
    
    func createDatabase()
    {
         fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            
            
            
        .appendingPathComponent("database.sqlite")
        
        fileURL1 = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        
    }
    
   
    func getUrl()->URL
    {
        return fileURL1!
    }
    
    func opendb()
    {
        createDatabase()
       
        if sqlite3_open(fileURL!.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        else{
            createTable()
            createBookMarkTable()
            createCategoryTable()
        }
    }
    func createTable()
    {
       
        var TEXT_TYPE = " TEXT";
        var  COMMA_SEP = ",";
        var END = ")"
        
//        var  SQL_CREATE_ENTRIES =
//        "CREATE TABLE " + TABLE_NAME + " (" +
//        _ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
//        COLUMN_NAME_LECTURE_TITLE + TEXT_TYPE + COMMA_SEP +
//        COLUMN_NAME_LECTURE_CATEGORY + TEXT_TYPE + COMMA_SEP +
//        COLUMN_NAME_LECTURE_DATE + TEXT_TYPE + COMMA_SEP +
//        COLUMN_NAME_LECTURE_DESC + TEXT_TYPE + COMMA_SEP +
//        COLUMN_NAME_LECTURE_BOOKMARK + " INTEGER " + COMMA_SEP +
//        COLUMN_NAME_RECORDING_NAME + TEXT_TYPE + COMMA_SEP +
//        COLUMN_NAME_RECORDING_FILE_PATH + TEXT_TYPE + COMMA_SEP +
//        COLUMN_NAME_RECORDING_LENGTH + " INTEGER " + COMMA_SEP +
//        COLUMN_NAME_TIME_START + " INTEGER " + COMMA_SEP +
//        COLUMN_NAME_TIME_END + " INTEGER " + COMMA_SEP +
//        COLUMN_NAME_PROCESS_ID + TEXT_TYPE + COMMA_SEP +
//        COLUMN_NAME_IS_DOWNLOADED + " INTEGER " + ")"
        
        var SQL_CREATE_ENTRIES1 = "CREATE TABLE IF NOT EXISTS saved_recordings ( id INTEGER PRIMARY KEY AUTOINCREMENT," +
        COLUMN_NAME_LECTURE_TITLE + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_LECTURE_CATEGORY + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_LECTURE_DATE + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_LECTURE_DESC + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_LECTURE_BOOKMARK + " INTEGER " + COMMA_SEP +
            COLUMN_NAME_RECORDING_NAME + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_RECORDING_FILE_PATH + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_RECORDING_LENGTH + " INTEGER " + COMMA_SEP +
            COLUMN_NAME_TIME_START +  TEXT_TYPE  + COMMA_SEP +
            COLUMN_NAME_TIME_END + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_PROCESS_ID + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_LANGUAGE + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_IS_CONVERTED + " INTEGER " +
            
            COLUMN_NAME_IS_DOWNLOADED + " INTEGER " + ")" ;
        

        
        if sqlite3_exec(db,SQL_CREATE_ENTRIES1, nil, nil, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error creating table: \(errmsg)")
        }
        
            
//        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS Heroes (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, powerrank INTEGER)", nil, nil, nil) != SQLITE_OK {
//            let errmsg = String(cString: sqlite3_errmsg(db)!)
//            print("error creating table: \(errmsg)")
//        }
        else
        {
            print("tabel created successfully")
            
            
            
        }
        
       
        
        
    }
    
    func createCategoryTable()
    {
        
        var TEXT_TYPE = " TEXT";
        var  COMMA_SEP = ",";
        var END = ")"
        
        //        var  SQL_CREATE_ENTRIES =
        //        "CREATE TABLE " + TABLE_NAME + " (" +
        //        _ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
        //        COLUMN_NAME_LECTURE_TITLE + TEXT_TYPE + COMMA_SEP +
        //        COLUMN_NAME_LECTURE_CATEGORY + TEXT_TYPE + COMMA_SEP +
        //        COLUMN_NAME_LECTURE_DATE + TEXT_TYPE + COMMA_SEP +
        //        COLUMN_NAME_LECTURE_DESC + TEXT_TYPE + COMMA_SEP +
        //        COLUMN_NAME_LECTURE_BOOKMARK + " INTEGER " + COMMA_SEP +
        //        COLUMN_NAME_RECORDING_NAME + TEXT_TYPE + COMMA_SEP +
        //        COLUMN_NAME_RECORDING_FILE_PATH + TEXT_TYPE + COMMA_SEP +
        //        COLUMN_NAME_RECORDING_LENGTH + " INTEGER " + COMMA_SEP +
        //        COLUMN_NAME_TIME_START + " INTEGER " + COMMA_SEP +
        //        COLUMN_NAME_TIME_END + " INTEGER " + COMMA_SEP +
        //        COLUMN_NAME_PROCESS_ID + TEXT_TYPE + COMMA_SEP +
        //        COLUMN_NAME_IS_DOWNLOADED + " INTEGER " + ")"
        
        var SQL_CREATE_ENTRIES1 = "CREATE TABLE IF NOT EXISTS Category ( id INTEGER PRIMARY KEY AUTOINCREMENT," +
            CATEGORYNAME + TEXT_TYPE + ")" ;
        
        
        
        if sqlite3_exec(db,SQL_CREATE_ENTRIES1, nil, nil, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error creating table: \(errmsg)")
        }
            
            
            //        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS Heroes (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, powerrank INTEGER)", nil, nil, nil) != SQLITE_OK {
            //            let errmsg = String(cString: sqlite3_errmsg(db)!)
            //            print("error creating table: \(errmsg)")
            //        }
        else
        {
            print("tabel created successfully")
            
            
            
        }
        
        
        
        
    }
    
    func createBookMarkTable()
    {
        
        var TEXT_TYPE = " TEXT";
        var  COMMA_SEP = ",";
        var END = ")"
        
        //        var  SQL_CREATE_ENTRIES =
        //        "CREATE TABLE " + TABLE_NAME + " (" +
        //        _ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
        //        COLUMN_NAME_LECTURE_TITLE + TEXT_TYPE + COMMA_SEP +
        //        COLUMN_NAME_LECTURE_CATEGORY + TEXT_TYPE + COMMA_SEP +
        //        COLUMN_NAME_LECTURE_DATE + TEXT_TYPE + COMMA_SEP +
        //        COLUMN_NAME_LECTURE_DESC + TEXT_TYPE + COMMA_SEP +
        //        COLUMN_NAME_LECTURE_BOOKMARK + " INTEGER " + COMMA_SEP +
        //        COLUMN_NAME_RECORDING_NAME + TEXT_TYPE + COMMA_SEP +
        //        COLUMN_NAME_RECORDING_FILE_PATH + TEXT_TYPE + COMMA_SEP +
        //        COLUMN_NAME_RECORDING_LENGTH + " INTEGER " + COMMA_SEP +
        //        COLUMN_NAME_TIME_START + " INTEGER " + COMMA_SEP +
        //        COLUMN_NAME_TIME_END + " INTEGER " + COMMA_SEP +
        //        COLUMN_NAME_PROCESS_ID + TEXT_TYPE + COMMA_SEP +
        //        COLUMN_NAME_IS_DOWNLOADED + " INTEGER " + ")"
        
        var SQL_CREATE_ENTRIES1 = "CREATE TABLE IF NOT EXISTS Bookmark ( id INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_NAME_LECTURE_TITLE + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_LECTURE_CATEGORY + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_LECTURE_DATE + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_LECTURE_DESC + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_LECTURE_BOOKMARK + " INTEGER " + COMMA_SEP +
            COLUMN_NAME_RECORDING_NAME + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_RECORDING_FILE_PATH + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_RECORDING_LENGTH + " INTEGER " + COMMA_SEP +
            COLUMN_NAME_TIME_START +  TEXT_TYPE  + COMMA_SEP +
            COLUMN_NAME_TIME_END + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_LANGUAGE + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_IS_CONVERTED + " INTEGER " +
            COLUMN_NAME_PROCESS_ID + TEXT_TYPE + COMMA_SEP + COLUMN_NAME_IS_DOWNLOADED + " INTEGER " + ")" ;
        
        
        
        if sqlite3_exec(db,SQL_CREATE_ENTRIES1, nil, nil, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error creating table: \(errmsg)")
        }
            
            
            //        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS Heroes (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, powerrank INTEGER)", nil, nil, nil) != SQLITE_OK {
            //            let errmsg = String(cString: sqlite3_errmsg(db)!)
            //            print("error creating table: \(errmsg)")
            //        }
        else
        {
            print("tabel created successfully")
            
            
            
        }
        
        
        
        
    }
    
    func saveData()
    {
         var stmt: OpaquePointer?
         let name = "uzair"
        let powerRanking = "1"
        let queryString = "INSERT INTO Heroes (name, powerrank) VALUES (?,?)"
        
        //preparing the query
        if sqlite3_prepare(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
            return
        }
        
        //binding the parameters
        if sqlite3_bind_text(stmt, 1, name, -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding name: \(errmsg)")
            return
        }
        
        if sqlite3_bind_int(stmt, 2, (powerRanking as NSString).intValue) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding name: \(errmsg)")
            return
        }
        
        //executing the query to insert values
        if sqlite3_step(stmt) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure inserting hero: \(errmsg)")
            return
        }
        
        readValues()
        
    }
    
    func readValues()
    {
        var lectureModel = [LectureModel]()
       

        //this is our select query
        let queryString = "SELECT * FROM saved_recordings"

        //statement pointer
        var stmt:OpaquePointer?

        
        if sqlite3_prepare(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert1111: \(errmsg)")
            return
        }
        
        //traversing through all the records
        while(sqlite3_step(stmt) == SQLITE_ROW){
            
            
            let id = sqlite3_column_int(stmt, 0)
            
            print("id is here \(id)")
            
            let title = String(cString: sqlite3_column_text(stmt, 1))
            let category = String(cString:sqlite3_column_text(stmt, 2))
            
            let lec = LectureModel()
            
            lec.setid(id: Int(id))
            lec.settitle(title: title)
            lec.setcategory(category: category)
            lec.setdate(date: String(cString:sqlite3_column_text(stmt, 3)))
            lec.setdecsricption(decsricption: String(cString:sqlite3_column_text(stmt, 4)))
            lec.setbookmark(bookmark: String(cString:sqlite3_column_text(stmt, 5)))
            lec.setname(name: String(cString:sqlite3_column_text(stmt, 6)))
            lec.setfilepath(filepath: String(cString:sqlite3_column_text(stmt, 7)))
//            lec.setlength(length: Int(sqlite3_column_int(stmt, 8)))
//            lec.settimestart(timestart:sqlite3_column_double(stmt, 9))
//           lec.settimeend(timeend: sqlite3_column_double(stmt, 10))
            lec.setprocessID(processID: String(cString:sqlite3_column_text(stmt, 11)))
            lec.setisDownaloded(isDownaloded: Int(sqlite3_column_int(stmt, 12)))
            
            
            
            
            
            

            //adding values to list
            lectureModel.append(lec)
        }



        for i in lectureModel {
           
            print("id is here \(i.getid())")
            print("title is here \(i.gettitle())")
            print("cat is here \(i.getcategory())")
             print("desc is here \(i.getdecsricption())")
            print("desc is bookmar \(i.getbookmark())")
            print("desc is name \(i.getname())")
            
            print("desc is filepath \(i.getfilepath())")
            print("desc is length \(i.getlength())")
            print("desc is timestrt \(i.gettimestart())")
            print("desc is timeend \(i.gettimeend())")
            print("desc is processid \(i.getprocessID())")
            print("desc is isdownlaoadable \(i.getisDownaloded())")
            
            
            
            
        }
        
    }
    
    func addRecording(title : String , category : String , recordingName : String , filePath : String , length : Double , timeStart : Double , processID : String , isconverted : Int , lang : String)
    {
        var stmt: OpaquePointer?
        
        
        
       
        let queryString = "INSERT INTO saved_recordings (\(COLUMN_NAME_LECTURE_TITLE),\(COLUMN_NAME_LECTURE_CATEGORY),\(COLUMN_NAME_LECTURE_DATE),\(COLUMN_NAME_LECTURE_DESC),\(COLUMN_NAME_LECTURE_BOOKMARK),\(COLUMN_NAME_RECORDING_NAME),\(COLUMN_NAME_RECORDING_FILE_PATH),\(COLUMN_NAME_RECORDING_LENGTH),\(COLUMN_NAME_TIME_START),\(COLUMN_NAME_TIME_END),\(COLUMN_NAME_PROCESS_ID),\(COLUMN_NAME_IS_DOWNLOADED),\(COLUMN_NAME_IS_CONVERTED),\(COLUMN_NAME_LANGUAGE)) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        
        //preparing the query
        if sqlite3_prepare(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
            return
        }
        
        //binding the parameters
        if sqlite3_bind_text(stmt, 1, title, -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding name1: \(errmsg)")
            return
        }
        
        if sqlite3_bind_text(stmt, 2, category, -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding name2: \(errmsg)")
            return
        }
        
        if sqlite3_bind_text(stmt, 3, Calendar.current.amSymbol, -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding name3: \(errmsg)")
            return
        }
        
        if sqlite3_bind_text(stmt, 4, "description", -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding name3: \(errmsg)")
            return
        }
        if sqlite3_bind_text(stmt, 5, "bookmark", -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding name4: \(errmsg)")
            return
        }
        if sqlite3_bind_text(stmt, 6, recordingName, -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding name6: \(errmsg)")
            return
        }
        if sqlite3_bind_text(stmt, 7, filePath, -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding name3: \(errmsg)")
            return
        }
        
      
        
//        sqlite3_bind_double(stmt, Int32, Double)
        
        if sqlite3_bind_double(stmt, 8, length) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding name7: \(errmsg)")
            return
        }
        
        if sqlite3_bind_double(stmt, 9, timeStart) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding name8: \(errmsg)")
            return
        }
        
        if sqlite3_bind_double(stmt, 10, timeStart) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding name9: \(errmsg)")
            return
        }
        
        if sqlite3_bind_text(stmt, 11, processID, -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding name11: \(errmsg)")
            return
        }
        if sqlite3_bind_text(stmt, 12, "1", -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding name11: \(errmsg)")
            return
        }
        
       
        
        
        
        //executing the query to insert values
        if sqlite3_step(stmt) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure inserting hero: \(errmsg)")
            return
        }
//
//        let queryString = "INSERT INTO saved_recordings (\(COLUMN_NAME_LECTURE_TITLE), \(COLUMN_NAME_LECTURE_CATEGORY),\(COLUMN_NAME_LECTURE_DATE),\(COLUMN_NAME_LECTURE_DESC),\(COLUMN_NAME_LECTURE_BOOKMARK),\(COLUMN_NAME_RECORDING_NAME),\(COLUMN_NAME_RECORDING_FILE_PATH),\(COLUMN_NAME_RECORDING_LENGTH),\(COLUMN_NAME_TIME_START),\(COLUMN_NAME_TIME_END),\(COLUMN_NAME_IS_DOWNLOADED),\(COLUMN_NAME_PROCESS_ID))) VALUES (\(title),\(category),\(Calendar.current.amSymbol),\(recordingName),\(filePath),\(length),\(timeStart),\(Calendar.current.amSymbol),\(0),\(processID))"
       
        
//        let queryString =  "INSERT INTO " + TABLE_NAME + " VALUES ('" + title + "','" + category + "','" + Calendar.current.amSymbol + "','" + "description" + "','" + "0" + "','" + recordingName + "','" + filePath + "','" + length + "','" + timeStart + "','" + Calendar.current.amSymbol + "', '" + 0 + "','" + processID + "')";
        
        
//        let queryString = "INSERT INTO " +
//            TABLE_NAME + " Values (1,'" + title + "' , '" + category + "' , '" + Calendar.current.amSymbol + "' ,   '" + "description" + "' , '" + "0" + "' , '" + recordingName + "', '" + filePath + "' , \(length) , \(timeStart) , '" + Calendar.current.amSymbol + "' , 0  , '" + processID + "' );"
        
//        "INSERT INTO " +
//            TABLE_NAME + " Values ('" + title + "');"
//            "INSERT INTO " +
//            TABLE_NAME + " Values ('" + title + "' , '" + category + "' , '" + Calendar.current.amSymbol + "' ,   '" + "description" + "' , '" + "0" + "' , '" + recordingName + "', '" + filePath + "' , '" + length + "' , '" + timeStart + "' , '" + Calendar.current.amSymbol + "' , '" + 0 + "' , '" + processID + "');" ;
        
        
        //preparing the query
        if sqlite3_prepare(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
            return
        }
        else{
         
             print("recording insert successfully")
        }
        
//        COLUMN_NAME_LECTURE_TITLE + TEXT_TYPE + COMMA_SEP +
//            COLUMN_NAME_LECTURE_CATEGORY + TEXT_TYPE + COMMA_SEP +
//            COLUMN_NAME_LECTURE_DATE + TEXT_TYPE + COMMA_SEP +
//            COLUMN_NAME_LECTURE_DESC + TEXT_TYPE + COMMA_SEP +
//            COLUMN_NAME_LECTURE_BOOKMARK + " INTEGER " + COMMA_SEP +
//            COLUMN_NAME_RECORDING_NAME + TEXT_TYPE + COMMA_SEP +
//            COLUMN_NAME_RECORDING_FILE_PATH + TEXT_TYPE + COMMA_SEP +
//            COLUMN_NAME_RECORDING_LENGTH + " INTEGER " + COMMA_SEP +
//            COLUMN_NAME_TIME_START + " INTEGER " + COMMA_SEP +
//            COLUMN_NAME_TIME_END + " INTEGER " + COMMA_SEP +
//            COLUMN_NAME_PROCESS_ID + TEXT_TYPE + COMMA_SEP + COLUMN_NAME_IS_DOWNLOADED
//
         readValues()
    }
    
    func openDatabase() {
        let path = Bundle.main.resourceURL
     //   let resourcePath = NSBundle.mainBundle().resourceURL!.absoluteString
        let pathDB = path?.appendingPathComponent(dbFileName).absoluteString
      //  let dbPath = path.stringByAppendingPathComponent(dbFileName)
        let database = FMDatabase(path: pathDB)
        
        /* Open database read-only. */
        if(!database.open(withFlags: 1))
        {
            print("could not open at (pathDB)")
        }
        else
        {
            self.database = database
            print("good")
        }
    }
    
    func getCategory()
    {
        
        let querySQL = "SELECT * FROM tbl_category"
        
       // let results:FMResultSet? = database!.executeQuery(querySQL,
                                                          // withArgumentsInArray: nil)
    }
    
    func closeDatabase() {
        if (database != nil) {
            database.close()
         
        }
    }
    
    func deleteDatabase(filePath : String)
    {
        let filemManager = FileManager.default
        do {
            let fileURL = NSURL(fileURLWithPath: filePath)
            try filemManager.removeItem(at: fileURL as URL)
            print("Database Deleted!")
        } catch {
            print("Error on Delete Database!!!")
        }
    }
    
   
    
    func deleteAll()
    {
         let querySQL = "SELECT * FROM tbl_category"
       
         var stmt: OpaquePointer?
        if sqlite3_prepare(db, querySQL, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
            return
        }
    }
    
  
}
