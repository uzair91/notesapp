//
//  FMDBDatabaseModel.swift
//  FMDBDatabase Demo
//
//  Created by Parth Changela on 22/06/17.
//  Copyright © 2017 Micropple. All rights reserved.
//

import Foundation
import UIKit
import FMDB



let sharedInstance = FMDBDatabaseModel()
class FMDBDatabaseModel: NSObject {

    var databese:FMDatabase? = nil
    public var  TABLE_NAME = "saved_recordings";
    
    public var  COLUMN_NAME_LECTURE_TITLE = "lecture_title";
    public var  COLUMN_NAME_LECTURE_CATEGORY = "lecture_category";
    public var COLUMN_NAME_LECTURE_DATE = "lecture_date";
    public var COLUMN_NAME_LECTURE_DESC = "lecture_desc";
    public var COLUMN_NAME_LECTURE_BOOKMARK = "lecture_bookmark";
    public var COLUMN_NAME_RECORDING_NAME = "recording_name";
    public var COLUMN_NAME_RECORDING_FILE_PATH = "file_path";
    public var COLUMN_NAME_RECORDING_LENGTH = "length";
    public var COLUMN_NAME_TIME_START = "time_start";
    public var COLUMN_NAME_TIME_END = "time_end";
    public var COLUMN_NAME_PROCESS_ID = "pid";
    public var COLUMN_NAME_IS_DOWNLOADED = "is_downloaded";
    public var CATEGORYNAME = "cat_name";
 
    
    

    class func getInstance() -> FMDBDatabaseModel
    {
        if (sharedInstance.databese == nil)
        {
        sharedInstance.databese = FMDatabase(path: Util.getPath(fileName: "database.sqlite"))
        }
        return sharedInstance
    }

    func InsertData(_ lectureModel : LectureModel) -> Bool {
        sharedInstance.databese!.open()
       
        let isInserted = sharedInstance.databese!.executeUpdate( "INSERT INTO saved_recordings (\(COLUMN_NAME_LECTURE_TITLE),\(COLUMN_NAME_LECTURE_CATEGORY),\(COLUMN_NAME_LECTURE_DATE),\(COLUMN_NAME_LECTURE_DESC),\(COLUMN_NAME_LECTURE_BOOKMARK),\(COLUMN_NAME_RECORDING_NAME),\(COLUMN_NAME_RECORDING_FILE_PATH),\(COLUMN_NAME_RECORDING_LENGTH),\(COLUMN_NAME_TIME_START),\(COLUMN_NAME_TIME_END),\(COLUMN_NAME_PROCESS_ID),\(COLUMN_NAME_IS_DOWNLOADED)) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)", withArgumentsIn: [lectureModel.gettitle(),lectureModel.getcategory(),lectureModel.getdate(),lectureModel.getdecsricption(),lectureModel.getbookmark(),lectureModel.getname(),lectureModel.getfilepath(),lectureModel.getlength(),lectureModel.timestart,lectureModel.gettimeend(),lectureModel.processID,lectureModel.getisDownaloded()])
        
        
        
        sharedInstance.databese!.close()
        return (isInserted != nil)
        
    }
    
    func getDescription(pid:String) -> String
    {
        var item:String = ""
        
        sharedInstance.databese!.open()
        
        let resultSet:FMResultSet! = sharedInstance.databese!.executeQuery("select * from saved_recordings where pid  = ?", withArgumentsIn: [pid])
        
        let itemInfo:NSMutableArray = NSMutableArray ()
        
        while resultSet.next() {
            
            do{
                item = String(resultSet.string(forColumn: "lecture_desc")!)
                
//                item = Int(resultSet.int(forColumn: "count"))
                
            }
            catch
            {
                
            }
            
        }
        
        sharedInstance.databese!.close()
        return item
        
    }
    
    func getPID(filepath:String) -> String
    {
        var item:String = ""
        
        sharedInstance.databese!.open()
        
        let resultSet:FMResultSet! = sharedInstance.databese!.executeQuery("select * from saved_recordings where file_path  = ?", withArgumentsIn: [filepath])
        
        let itemInfo:NSMutableArray = NSMutableArray ()
        
        while resultSet.next() {
            
            do{
                item = String(resultSet.string(forColumn: "pid")!)
                
                //                item = Int(resultSet.int(forColumn: "count"))
                
            }
            catch
            {
                
            }
            
        }
        
        sharedInstance.databese!.close()
        return item
        
    }
    
    func getLanguageCode(pid:String) -> Int
    {
        var item:Int = 0
        
        sharedInstance.databese!.open()
        
        let resultSet:FMResultSet! = sharedInstance.databese!.executeQuery("select * from saved_recordings where pid = ?", withArgumentsIn: [pid])
        
        let itemInfo:NSMutableArray = NSMutableArray ()
        
        while resultSet.next() {
            
            do{
                item = Int(resultSet.int(forColumn: "is_downloaded"))
                
                print("item is here \(item)")

                
                //                item = Int(resultSet.int(forColumn: "count"))
                
            }
            catch
            {
                
            }
            
        }
        
        sharedInstance.databese!.close()
        return item
        
    }
    
    func updatePID(pid : String,filepath: String) -> Bool
    {
        sharedInstance.databese!.open()
        
        var check  : Bool = true
        
        
        do {
            try sharedInstance.databese!.executeUpdate("UPDATE saved_recordings SET pid = ? WHERE file_path = ?", withArgumentsIn: [pid,filepath])
            
            check = true
        }
        catch
        {
            check = false
        }
        
        
        
        sharedInstance.databese!.close()
        return check
    }
    
    func InsertDataCategory(cat_name : String ) -> Bool {
        sharedInstance.databese!.open()
        
//        let isInserted =
        
        var check  : Bool = true
        
        
        do {
            try sharedInstance.databese!.executeUpdate( "INSERT INTO Category (\(CATEGORYNAME)) VALUES (?)", withArgumentsIn: [cat_name])
            
            check = true
        }
        catch
        {
            check = false
        }
        
        
        sharedInstance.databese!.close()
        return check
        
    }
    
    func InsertDatainBookmark(_ lectureModel : LectureModel) -> Bool {
        sharedInstance.databese!.open()
        
        
        var check  : Bool = true
        
        
       
        
        let isInserted = sharedInstance.databese!.executeUpdate( "INSERT INTO Bookmark (\(COLUMN_NAME_LECTURE_TITLE),\(COLUMN_NAME_LECTURE_CATEGORY),\(COLUMN_NAME_LECTURE_DATE),\(COLUMN_NAME_LECTURE_DESC),\(COLUMN_NAME_LECTURE_BOOKMARK),\(COLUMN_NAME_RECORDING_NAME),\(COLUMN_NAME_RECORDING_FILE_PATH),\(COLUMN_NAME_RECORDING_LENGTH),\(COLUMN_NAME_TIME_START),\(COLUMN_NAME_TIME_END),\(COLUMN_NAME_PROCESS_ID),\(COLUMN_NAME_IS_DOWNLOADED)) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)", withArgumentsIn: [lectureModel.gettitle(),lectureModel.getcategory(),lectureModel.getdate(),lectureModel.getdecsricption(),lectureModel.getbookmark(),lectureModel.getname(),lectureModel.getfilepath(),lectureModel.getlength(),lectureModel.timestart,lectureModel.gettimeend(),lectureModel.processID,lectureModel.getisDownaloded()])
        
        
        
        sharedInstance.databese!.close()
        return (isInserted != nil)
        
    }
    
    func GetAllData() -> NSMutableArray {
        sharedInstance.databese!.open()
        
        let resultSet:FMResultSet! = sharedInstance.databese!.executeQuery("SELECT * FROM saved_recordings", withArgumentsIn: [0])
        
        let itemInfo:NSMutableArray = NSMutableArray ()
        if (resultSet != nil)
        {
            while resultSet.next() {
                
                let item:LectureModel = LectureModel()
                
                
                
                do{
                    
                   
                
                    
                    
                    item.id = Int(resultSet.int(forColumn: "Id"))
                    item.title = String(resultSet.string(forColumn: "lecture_title")!)
                    item.category = String(resultSet.string(forColumn: "lecture_category")!)
                    item.date = String(resultSet.string(forColumn: "lecture_date")!)
                    item.decsricption = String(resultSet.string(forColumn: "lecture_desc")!)
                    //                item.decsricption = String(resultSet.string(forColumn: "lecture_desc")!)
                    item.length = String(resultSet.string(forColumn: "length")!)
                    item.filepath = String(resultSet.string(forColumn: "file_path")!)
                    item.processID = String(resultSet.string(forColumn: "pid")!)
                    itemInfo.add(item)
                }
                catch
                {
                    
                }
               
            }
        }
        
        sharedInstance.databese!.close()
        return itemInfo
    }
    
    func GetAllDataCateogry() -> NSMutableArray {
        sharedInstance.databese!.open()
        
        let resultSet:FMResultSet! = sharedInstance.databese!.executeQuery("SELECT * FROM Category", withArgumentsIn: [0])
        
        let itemInfo:NSMutableArray = NSMutableArray ()
        if (resultSet != nil)
        {
            while resultSet.next() {
                
                let item:CatgegoryModel = CatgegoryModel()
                
                
                
                do{
                    
                    
                    
                    
                    
                    item.cat_name = String(resultSet.string(forColumn: "cat_name")!)
                    item.id = Int(resultSet.int(forColumn: "Id"))
//                    item.title = String(resultSet.string(forColumn: "lecture_title")!)
//                    item.category = String(resultSet.string(forColumn: "lecture_category")!)
//                    item.date = String(resultSet.string(forColumn: "lecture_date")!)
//                    item.decsricption = String(resultSet.string(forColumn: "lecture_desc")!)
//                    //                item.decsricption = String(resultSet.string(forColumn: "lecture_desc")!)
//                    item.length = String(resultSet.string(forColumn: "length")!)
//                    item.filepath = String(resultSet.string(forColumn: "file_path")!)
//                    item.processID = String(resultSet.string(forColumn: "pid")!)
                    itemInfo.add(item)
                }
                catch
                {
                    
                }
                
            }
        }
        
        sharedInstance.databese!.close()
        return itemInfo
    }
    
    func updateCategory(descritpion : String,id: Int) -> Bool
    {
        sharedInstance.databese!.open()
        
        var check  : Bool = true
        
        
        do {
            try sharedInstance.databese!.executeUpdate("UPDATE Category SET cat_name = ? WHERE id = ?", withArgumentsIn: [descritpion,id])
            
            check = true
        }
        catch
        {
            check = false
        }
        
        
        
        sharedInstance.databese!.close()
        return check
    }
    
    func GetAllDatabookmark() -> NSMutableArray {
        sharedInstance.databese!.open()
        
        let resultSet:FMResultSet! = sharedInstance.databese!.executeQuery("SELECT * FROM Bookmark", withArgumentsIn: [0])
        
        let itemInfo:NSMutableArray = NSMutableArray ()
        if (resultSet != nil)
        {
            while resultSet.next() {
                
                let item:LectureModel = LectureModel()
                
                
                
                do{
                    
                    
                    
                    
                    
                    item.id = Int(resultSet.int(forColumn: "Id"))
                    item.title = String(resultSet.string(forColumn: "lecture_title")!)
                    item.category = String(resultSet.string(forColumn: "lecture_category")!)
                    item.date = String(resultSet.string(forColumn: "lecture_date")!)
                    item.decsricption = String(resultSet.string(forColumn: "lecture_desc")!)
                    //                item.decsricption = String(resultSet.string(forColumn: "lecture_desc")!)
                    item.length = String(resultSet.string(forColumn: "length")!)
                    item.filepath = String(resultSet.string(forColumn: "file_path")!)
                    item.processID = String(resultSet.string(forColumn: "pid")!)
                    itemInfo.add(item)
                }
                catch
                {
                    
                }
                
            }
        }
        
        sharedInstance.databese!.close()
        return itemInfo
    }
    
    
    func getData()
    {
        sharedInstance.databese!.open()
        
        let resultSet:FMResultSet! = sharedInstance.databese!.executeQuery("SELECT * FROM saved_recordings", withArgumentsIn: [0])
        
        let itemInfo:NSMutableArray = NSMutableArray ()
        if (resultSet != nil)
        {
            while resultSet.next() {
                
                let item:Tbl_Info = Tbl_Info()
                
                
                item.Id = Int(resultSet.int(forColumn: "Id"))
                
                print("id is here \(String(item.Id))")
                
//                item.Name = String(resultSet.string(forColumn: "Name")!)
//                item.MobileNo = String(resultSet.string(forColumn: "MobileNo")!)
//                item.Email = String(resultSet.string(forColumn: "Email")!)
                itemInfo.add(item)
            }
        }
        
        sharedInstance.databese!.close()
    }
    
    func getLecturesFromTitle(title : String) -> NSMutableArray
    {
        sharedInstance.databese!.open()
        
        let resultSet:FMResultSet! = sharedInstance.databese!.executeQuery("SELECT * FROM saved_recordings where lecture_title = ?", withArgumentsIn: [title])
        
        let itemInfo:NSMutableArray = NSMutableArray ()
        
        while resultSet.next() {
            
            let item:LectureModel = LectureModel()
            
            
            
            do{
                
                
                
                
                
                item.id = Int(resultSet.int(forColumn: "Id"))
                item.title = String(resultSet.string(forColumn: "lecture_title")!)
                item.category = String(resultSet.string(forColumn: "lecture_category")!)
                item.date = String(resultSet.string(forColumn: "lecture_date")!)
                item.decsricption = String(resultSet.string(forColumn: "lecture_desc")!)
                //                item.decsricption = String(resultSet.string(forColumn: "lecture_desc")!)
                item.length = String(resultSet.string(forColumn: "length")!)
                item.filepath = String(resultSet.string(forColumn: "file_path")!)
                item.processID = String(resultSet.string(forColumn: "pid")!)
                itemInfo.add(item)
            }
            catch
            {
                
            }
            
        }
        //
        //        if (resultSet != nil)
        //        {
        //            while resultSet.next() {
        //
        //                let item:Tbl_Info = Tbl_Info()
        //
        //
        //                item.Id = Int(resultSet.int(forColumn: "Id"))
        //
        //                print("id is here \(String(item.Id))")
        //
        //                //                item.Name = String(resultSet.string(forColumn: "Name")!)
        //                //                item.MobileNo = String(resultSet.string(forColumn: "MobileNo")!)
        //                //                item.Email = String(resultSet.string(forColumn: "Email")!)
        //                itemInfo.add(item)
        //            }
        //        }
        
        sharedInstance.databese!.close()
        return itemInfo
        
    }
    
    func getBoomarkCount(pid:String) -> Int
    {
        var item:Int = 0
        
        sharedInstance.databese!.open()
        
         let resultSet:FMResultSet! = sharedInstance.databese!.executeQuery("select count(*) as count from Bookmark where pid  = ?", withArgumentsIn: [pid])
        
          let itemInfo:NSMutableArray = NSMutableArray ()
        
         while resultSet.next() {
            
            do{
          
                
                item = Int(resultSet.int(forColumn: "count"))
             
            }
            catch
            {
                
            }
            
        }
        
         sharedInstance.databese!.close()
        return item
        
    }
    
    
    func getLecturesFromDate(date : String) -> NSMutableArray
    {
        sharedInstance.databese!.open()
        
        let resultSet:FMResultSet! = sharedInstance.databese!.executeQuery("SELECT * FROM saved_recordings where lecture_date = ?", withArgumentsIn: [date])
        
        let itemInfo:NSMutableArray = NSMutableArray ()
        
        while resultSet.next() {
            
            let item:LectureModel = LectureModel()
            
            
            
            do{
                
                
                
                
                
                item.id = Int(resultSet.int(forColumn: "Id"))
                item.title = String(resultSet.string(forColumn: "lecture_title")!)
                item.category = String(resultSet.string(forColumn: "lecture_category")!)
                item.date = String(resultSet.string(forColumn: "lecture_date")!)
                item.decsricption = String(resultSet.string(forColumn: "lecture_desc")!)
                //                item.decsricption = String(resultSet.string(forColumn: "lecture_desc")!)
                item.length = String(resultSet.string(forColumn: "length")!)
                item.filepath = String(resultSet.string(forColumn: "file_path")!)
                item.processID = String(resultSet.string(forColumn: "pid")!)
                itemInfo.add(item)
            }
            catch
            {
                
            }
            
        }
//
//        if (resultSet != nil)
//        {
//            while resultSet.next() {
//
//                let item:Tbl_Info = Tbl_Info()
//
//
//                item.Id = Int(resultSet.int(forColumn: "Id"))
//
//                print("id is here \(String(item.Id))")
//
//                //                item.Name = String(resultSet.string(forColumn: "Name")!)
//                //                item.MobileNo = String(resultSet.string(forColumn: "MobileNo")!)
//                //                item.Email = String(resultSet.string(forColumn: "Email")!)
//                itemInfo.add(item)
//            }
//        }
        
        sharedInstance.databese!.close()
         return itemInfo
        
    }
    
    func updateRecode(RecoreId:Int,Name:String,MobileNo:String,Email:String) -> NSMutableArray {
        sharedInstance.databese!.open()
        
        let resultSet:FMResultSet! = sharedInstance.databese!.executeQuery("UPDATE Info SET Name = ?,MobileNo = ?,Email = ? WHERE Id = ?", withArgumentsIn: [Name,MobileNo,Email,RecoreId])
        
        let itemInfo:NSMutableArray = NSMutableArray ()
        if (resultSet != nil)
        {
            while resultSet.next() {
                
                let item:Tbl_Info = Tbl_Info()
                item.Id = Int(resultSet.int(forColumn: "Id"))
                item.Name = String(resultSet.string(forColumn: "Name")!)
                item.MobileNo = String(resultSet.string(forColumn: "MobileNo")!)
                item.Email = String(resultSet.string(forColumn: "Email")!)
                itemInfo.add(item)
            }
        }
        
        sharedInstance.databese!.close()
        return itemInfo
        
    }
    
    
    func updateDescription(descritpion : String,pid : String) -> Bool
    {
        sharedInstance.databese!.open()
        
        var check  : Bool = true
        
        
        do {
            try sharedInstance.databese!.executeUpdate("UPDATE saved_recordings SET lecture_desc = ? WHERE pid = ?", withArgumentsIn: [descritpion,pid])
            
            if(getBoomarkCount(pid: pid)==1)
            
            {
                
                
               updateBookmark(descritpion: descritpion, pid: pid)
            }
            
            
            check = true
        }
        catch
        {
            check = false
        }
        
        
        
        sharedInstance.databese!.close()
        return check
    }
    
    func updateBookmark(descritpion : String,pid : String) -> Bool
    {
        sharedInstance.databese!.open()
        
        var check  : Bool = true
        
        
        do {
            try sharedInstance.databese!.executeUpdate("UPDATE Bookmark SET lecture_desc = ? WHERE pid = ?", withArgumentsIn: [descritpion,pid])
            
           
            
            
            check = true
        }
        catch
        {
            check = false
        }
        
        
        
        sharedInstance.databese!.close()
        return check
    }
    
    func updateTitle(descritpion : String,pid : String) -> Bool
    {
        sharedInstance.databese!.open()
        
        var check  : Bool = true
        
        
        do {
            try sharedInstance.databese!.executeUpdate("UPDATE saved_recordings SET lecture_title = ? WHERE pid = ?", withArgumentsIn: [descritpion,pid])
            
         
            
            if(getBoomarkCount(pid: pid)==1)
            {
                
                updateTitleBookmark(descritpion: descritpion, pid: pid)
                
            }
            
            check = true
        }
        catch
        {
            check = false
        }

        
      
       
        sharedInstance.databese!.close()
         return check
    }
    
    
    func updateTitleBookmark(descritpion : String,pid : String) -> Bool
    {
        sharedInstance.databese!.open()
        
        var check  : Bool = true
        
        
        do {
            try sharedInstance.databese!.executeUpdate("UPDATE Bookmark SET lecture_title = ? WHERE pid = ?", withArgumentsIn: [descritpion,pid])
            
            
            
            check = true
        }
        catch
        {
            check = false
        }
        
        
        
        
        sharedInstance.databese!.close()
        return check
    }
    
    func deleteRecordFromBookmark(pid : String)-> Bool
    {
        sharedInstance.databese!.open()
        
        var check  : Bool = true
        
        do {
            
            try sharedInstance.databese!.executeUpdate(" DELETE FROM Bookmark WHERE pid = ?", withArgumentsIn: [pid])
            
            
            
            //            try sharedInstance.databese!.executeUpdate(" DELETE FROM  Bookmark WHERE pid = ?", withArgumentsIn: [pid])
            
            
            
            check = true
        }
        catch
        {
            check = false
        }
        
        sharedInstance.databese!.close()
        
        return check
    }
    
    
    func deleteRecordFromCateogry(pid : Int)-> Bool
    {
        sharedInstance.databese!.open()
        
        var check  : Bool = true
        
        do {
            
            try sharedInstance.databese!.executeUpdate(" DELETE FROM Category WHERE id = ?", withArgumentsIn: [pid])
            
            
            
//            try sharedInstance.databese!.executeUpdate(" DELETE FROM  Bookmark WHERE pid = ?", withArgumentsIn: [pid])
            
            
            
            check = true
        }
        catch
        {
            check = false
        }
     
        sharedInstance.databese!.close()
        
        return check
    }
    
    func deleteRecordFromRecording(pid : String)-> Bool
    {
        sharedInstance.databese!.open()
        
        var check  : Bool = true
        
        do {
            
            try sharedInstance.databese!.executeUpdate(" DELETE FROM saved_recordings WHERE pid = ?", withArgumentsIn: [pid])
            
            
            
            try sharedInstance.databese!.executeUpdate(" DELETE FROM  Bookmark WHERE pid = ?", withArgumentsIn: [pid])
            
           
            
            check = true
        }
        catch
        {
            check = false
        }
        
        
       
        
        
        
         sharedInstance.databese!.close()
        
        return check
    }
    
    func deleteRecode(RecoreId:Int) -> NSMutableArray {
        sharedInstance.databese!.open()
        
        let resultSet:FMResultSet! = sharedInstance.databese!.executeQuery("DELETE FROM Info WHERE Id = ?", withArgumentsIn: [RecoreId])
        
        let itemInfo:NSMutableArray = NSMutableArray ()
        if (resultSet != nil)
        {
            while resultSet.next() {
                
                let item:Tbl_Info = Tbl_Info()
                item.Id = Int(resultSet.int(forColumn: "Id"))
                item.Name = String(resultSet.string(forColumn: "Name")!)
                item.MobileNo = String(resultSet.string(forColumn: "MobileNo")!)
                item.Email = String(resultSet.string(forColumn: "Email")!)
                itemInfo.add(item)
            }
        }
        
        sharedInstance.databese!.close()
        return itemInfo
        
    }
    //MARK:- insert data into table

//    func InsertData(_ Tbl_Info:Tbl_Info) -> Bool {
//        sharedInstance.databese!.open()
//        let isInserted = sharedInstance.databese!.executeUpdate("INSERT INTO Info(Name,MobileNo,Email) VALUES(?,?,?)", withArgumentsIn: [Tbl_Info.Name,Tbl_Info.MobileNo,Tbl_Info.Email])
//
//        sharedInstance.databese!.close()
//        return (isInserted != nil)
//
//    }
//    func GetAllData() -> NSMutableArray {
//        sharedInstance.databese!.open()
//
//        let resultSet:FMResultSet! = sharedInstance.databese!.executeQuery("SELECT * FROM Info", withArgumentsIn: [0])
//
//        let itemInfo:NSMutableArray = NSMutableArray ()
//        if (resultSet != nil)
//        {
//            while resultSet.next() {
//
//                let item:Tbl_Info = Tbl_Info()
//                item.Id = Int(resultSet.int(forColumn: "Id"))
//                item.Name = String(resultSet.string(forColumn: "Name")!)
//                item.MobileNo = String(resultSet.string(forColumn: "MobileNo")!)
//                item.Email = String(resultSet.string(forColumn: "Email")!)
//                itemInfo.add(item)
//            }
//        }
//
//        sharedInstance.databese!.close()
//        return itemInfo
//    }
//
//    func updateRecode(RecoreId:Int,Name:String,MobileNo:String,Email:String) -> NSMutableArray {
//        sharedInstance.databese!.open()
//
//        let resultSet:FMResultSet! = sharedInstance.databese!.executeQuery("UPDATE Info SET Name = ?,MobileNo = ?,Email = ? WHERE Id = ?", withArgumentsIn: [Name,MobileNo,Email,RecoreId])
//
//        let itemInfo:NSMutableArray = NSMutableArray ()
//        if (resultSet != nil)
//        {
//            while resultSet.next() {
//
//                let item:Tbl_Info = Tbl_Info()
//                item.Id = Int(resultSet.int(forColumn: "Id"))
//                item.Name = String(resultSet.string(forColumn: "Name")!)
//                item.MobileNo = String(resultSet.string(forColumn: "MobileNo")!)
//                item.Email = String(resultSet.string(forColumn: "Email")!)
//                itemInfo.add(item)
//            }
//        }
//
//        sharedInstance.databese!.close()
//        return itemInfo
//
//    }
//    func deleteRecode(RecoreId:Int) -> NSMutableArray {
//        sharedInstance.databese!.open()
//
//        let resultSet:FMResultSet! = sharedInstance.databese!.executeQuery("DELETE FROM Info WHERE Id = ?", withArgumentsIn: [RecoreId])
//
//        let itemInfo:NSMutableArray = NSMutableArray ()
//        if (resultSet != nil)
//        {
//            while resultSet.next() {
//
//                let item:Tbl_Info = Tbl_Info()
//                item.Id = Int(resultSet.int(forColumn: "Id"))
//                item.Name = String(resultSet.string(forColumn: "Name")!)
//                item.MobileNo = String(resultSet.string(forColumn: "MobileNo")!)
//                item.Email = String(resultSet.string(forColumn: "Email")!)
//                itemInfo.add(item)
//            }
//        }
//
//        sharedInstance.databese!.close()
//        return itemInfo
//
//    }
}
