//
//  Recording.swift
//  Audio Recorder
//
//  Created by Venkat Kukunuru on 30/12/16.
//  Copyright © 2016 Venkat Kukunuru. All rights reserved.
//

import Foundation
import AVFoundation
import QuartzCore

@objc public protocol RecorderDelegate: AVAudioRecorderDelegate {
    @objc optional func audioMeterDidUpdate(_ dB: Float)
}

open class Recording : NSObject {
    
    @objc public enum State: Int {
        case none, record, play
    }
    
    static var directory: String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    open weak var delegate: RecorderDelegate?
    open fileprivate(set) var url: URL
    open fileprivate(set) var state: State = .none
    
    open var bitRate = 128000
    open var sampleRate = 44100
    open var channels = 1
    
    var playerItem : AVPlayerItem?
    fileprivate let session = AVAudioSession.sharedInstance()
    var recorder: AVAudioRecorder?
    fileprivate var player: AVPlayer?
    fileprivate var link: CADisplayLink?
    
    var metering: Bool {
        return delegate?.responds(to: #selector(RecorderDelegate.audioMeterDidUpdate(_:))) == true
    }
    
    // MARK: - Initializers
    
    public init(to: String) {
        
//        fileManager.contentsOfDirectory
        
      
        
      

        print("uzair b here")
        url = URL(fileURLWithPath: Recording.directory).appendingPathComponent(to)
        
      
//        url = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
//            .appendingPathComponent(to)
//
        
//        url =URL(fileURLWithPath:url.relativePath)
//        url = URL(fileURLWithPath:Bundle.main.path(forResource: "bensound-sunny.mp3", ofType:nil)!)
        super.init()
        
    }
    
    public func getUrl()->URL
        {
             print("uzair b here")
            return self.url ;
        }
    
    // MARK: - Record
    
    
    
    open func prepare() throws {
        
        print("uzair a here")
        
        
          try session.setCategory(AVAudioSessionCategoryPlayAndRecord, with: AVAudioSessionCategoryOptions.defaultToSpeaker)
        
        let settings: [String: AnyObject] = [
            AVFormatIDKey : NSNumber(value: Int32(kAudioFormatMPEG4AAC) as Int32),
            AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue as AnyObject,
            AVEncoderBitRateKey: bitRate as AnyObject,
            AVNumberOfChannelsKey: channels as AnyObject,
            AVSampleRateKey: sampleRate as AnyObject
        ]
        
       
        
        recorder = try AVAudioRecorder(url: url, settings: settings)
        recorder?.prepareToRecord()
        recorder?.delegate = delegate
        recorder?.isMeteringEnabled = metering
        
    }
    
    open func record() throws {
        if recorder == nil {
            try prepare()
       
        }
        
        print("hello uzair here")
        
      
//        try  session.setCategory(AVAudioSessionCategoryPlayAndRecord)
//        try session.overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
        
        
       

         try session.setCategory(AVAudioSessionCategoryPlayAndRecord, with: AVAudioSessionCategoryOptions.defaultToSpeaker)
        
        recorder?.record()
        state = .record
        
        if metering {
            startMetering()
        }
    }
    
    open func pause()  {
        
        
//        try session.setCategory(AVAudioSessionCategoryPlayAndRecord)
//        try session.overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
        
        recorder?.pause()
        
//        recorder?.record()
//        state = .record
        
//        if metering {
//            startMetering()
//        }
    }
    
    open func resume()
    {
        
    }
    
    
  
    // MARK: - Playback
    
    open func play() throws {
        
        
        
        print("uzair b here")
        
        try session.setCategory(AVAudioSessionCategoryPlayAndRecord, with: AVAudioSessionCategoryOptions.defaultToSpeaker)
        
//        session.setActive(true, error: &error)
//        try session.setCategory(AVAudioSessionCategoryPlayAndRecord)
       
//        player = try AVAudioPlayer(contentsOf: url)
//        player.audioTimePitchAlgorithm = AVAudioTimePitchAlgorithm.spectral
        
//        prepareToPlay()
        
        player = try AVPlayer(url: url)
        player?.play()
        state = .play
        
       
        
        
    }
    
    func prepareToPlay() {
        
        // Create asset to be played
        print("url open is here \(url)")
        print("uzair b here")
        
        let assetKeys = [
            "playable",
            "hasProtectedContent"
        ]
        // Create a new AVPlayerItem with the asset and an
        // array of asset keys to be automatically loaded
        playerItem = AVPlayerItem(asset: AVAsset(url: url),
                                  automaticallyLoadedAssetKeys: assetKeys)
        
        // Register as an observer of the player item's status property
        playerItem!.addObserver(self,
                               forKeyPath: #keyPath(AVPlayerItem.status),
                               options: [.old, .new],
                               context: &playerItem)
        
        // Associate the player item with the player
        player = AVPlayer(playerItem: playerItem)
    }
    
    open func stop() {
        switch state {
        case .play:
            player?.pause()
            player = nil
        case .record:
            recorder?.stop()
            recorder = nil
            stopMetering()
        default:
            break
        }
        
        state = .none
    }
    
    override open func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {
        // Only handle observations for the playerItemContext
        
        print("uzair b here")
        guard context == &playerItem else {
            super.observeValue(forKeyPath: keyPath,
                               of: object,
                               change: change,
                               context: context)
            return
        }
        
        if keyPath == #keyPath(AVPlayerItem.status) {
            let status: AVPlayerItemStatus
            
            // Get the status change from the change dictionary
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItemStatus(rawValue: statusNumber.intValue)!
            } else {
                status = .unknown
            }
            
            // Switch over the status
            switch status {
            case .readyToPlay: break
            // Player item is ready to play.
            case .failed: break
            // Player item failed. See error.
            case .unknown: break
                // Player item is not yet ready.
            }
        }
    }
    
    // MARK: - Metering
    
    @objc func updateMeter() {
        
        print("uzair b here")
        guard let recorder = recorder else { return }
        
        recorder.updateMeters()
        
        let dB = recorder.averagePower(forChannel: 0)
        
        delegate?.audioMeterDidUpdate?(dB)
    }
    
    fileprivate func startMetering() {
        print("uzair b here")
        link = CADisplayLink(target: self, selector: #selector(Recording.updateMeter))
        link?.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
    }
    
    fileprivate func stopMetering() {
        print("uzair b here")
        link?.invalidate()
        link = nil
    }
    
    
    
    
    
    
}
