//
//  SharedHelper.swift
//  SwiftProject
//
//  Created by Munzareen Atique on 08/01/2018.
//  Copyright © 2018 Munzareen Atique. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration
import iProgressHUD
import Alamofire

class SharedHelper: UIViewController  {
    
    let iprogress: iProgressHUD = iProgressHUD()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    func showSpinner(view : UIView) {
        
        
        iprogress.isShowModal = true
        iprogress.isShowCaption = true
        iprogress.isTouchDismiss = false
        iprogress.indicatorStyle = .circleStrokeSpin
        iprogress.indicatorColor = .white
        iprogress.iprogressStyle = .horizontal
        iprogress.indicatorView.startAnimating()
        
        iprogress.attachProgress(toView: view)
        view.showProgress()
        
    }
    func hideSpinner(view : UIView) {
        iprogress.indicatorView.stopAnimating()
        view.dismissProgress()
    }
    
  
    func showToast(message: String, controller: UIViewController) {
        let toastContainer = UIView(frame: CGRect())
        toastContainer.backgroundColor = UIColor.black
        toastContainer.alpha = 0.0
        toastContainer.layer.cornerRadius = 10;
        toastContainer.clipsToBounds  =  true
        
        let toastLabel = UILabel(frame: CGRect())
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font.withSize(12.0)
        toastLabel.text = message
        toastLabel.clipsToBounds  =  true
        toastLabel.numberOfLines = 0
        
        toastContainer.addSubview(toastLabel)
        controller.view.addSubview(toastContainer)
        
        toastLabel.translatesAutoresizingMaskIntoConstraints = false
        toastContainer.translatesAutoresizingMaskIntoConstraints = false
        
        let a1 = NSLayoutConstraint(item: toastLabel, attribute: .leading, relatedBy: .equal, toItem: toastContainer, attribute: .leading, multiplier: 1, constant: 15)
        let a2 = NSLayoutConstraint(item: toastLabel, attribute: .trailing, relatedBy: .equal, toItem: toastContainer, attribute: .trailing, multiplier: 1, constant: -15)
        let a3 = NSLayoutConstraint(item: toastLabel, attribute: .bottom, relatedBy: .equal, toItem: toastContainer, attribute: .bottom, multiplier: 1, constant: -15)
        let a4 = NSLayoutConstraint(item: toastLabel, attribute: .top, relatedBy: .equal, toItem: toastContainer, attribute: .top, multiplier: 1, constant: 15)
        toastContainer.addConstraints([a1, a2, a3, a4])
        
        let c1 = NSLayoutConstraint(item: toastContainer, attribute: .leading, relatedBy: .equal, toItem: controller.view, attribute: .leading, multiplier: 1, constant: 65)
        let c2 = NSLayoutConstraint(item: toastContainer, attribute: .trailing, relatedBy: .equal, toItem: controller.view, attribute: .trailing, multiplier: 1, constant: -65)
        let c3 = NSLayoutConstraint(item: toastContainer, attribute: .bottom, relatedBy: .equal, toItem: controller.view, attribute: .bottom, multiplier: 1, constant: -75)
        controller.view.addConstraints([c1, c2, c3])
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            toastContainer.alpha = 1.0
        }, completion: { _ in
            UIView.animate(withDuration: 0.5, delay: 1.5, options: .curveEaseOut, animations: {
                toastContainer.alpha = 0.0
            }, completion: {_ in
                toastContainer.removeFromSuperview()
            })
        })
    }
    
    
    func PostRequest(url:String, parameters:Parameters, completion: @escaping (_ result: DataResponse<Any>) -> Void) {
        
        Alamofire.request(url, method: .post,parameters: parameters).responseJSON
            { response in
                
                switch response.result {
                case .success:
                    print(response)
                    completion(response)
                    break
                case .failure(let error):
                    print(error)
                    completion(response)
                }
                
        }
    }
    
    func GetRequest(url:String, completion: @escaping (_ result: DataResponse<Any>) -> Void) {
        
        
        
        let headers: HTTPHeaders = [
            "api-key": "357f4ac483e7fc58a8cc1f588c639e79"
            
        ]
        Alamofire.request(url, method: .get, headers: headers).responseJSON{
            //        Alamofire.request(url, method: .post, parameters: parameters).responseJSON
            response in
            
            switch response.result {
            case .success:
                
               
//                let responseData = response.result.value as! NSDictionary
                print(response)
                completion(response)
                break
            case .failure(let error):
                print(error)
                completion(response)
            }
            
        }
    }
    
    func addAudioFile(url:String, audioData:Data, parameters:Parameters, completion: @escaping (_ result: DataResponse<Any>) -> Void) {
        
//        let authentication = UserDefaults.standard.string(forKey: "auth_token")
//        let auth_value = UserDefaults.standard.string(forKey: "auth_value")
//
//        let headers: HTTPHeaders = [
//            "Authentication" : authentication! ,
//            "Value" : auth_value!
//        ]
        
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 3600
        configuration.timeoutIntervalForResource = 3600
        
        
//        Alamofire.request(url, method: .post, parameters: parameters, encoding: <#T##ParameterEncoding#>, headers: <#T##HTTPHeaders?#>).responseJSON { (<#DataResponse<Any>#>) in
//
//
//        }
        
        
      
        
       
       let alamoFireManager = Alamofire.SessionManager(configuration: configuration)
    
        
       
        
        alamoFireManager.upload(multipartFormData:
            {
                (multipartFormData) in
                
           
//                multipartFormData.append(audioData, withName: "file", fileName: "recording.mp4", mimeType: "audio/m4a")
             
      multipartFormData.append(audioData, withName: "file", fileName: "recording.mp3", mimeType: "audio/m4a")
//mimeType: "application/octet-stream"
                
                
                for (key, value) in parameters
                {
                    
//                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                
                }
               
        }, to:url)
        { (result) in
            switch result {
            case .success(let upload,_,_ ):
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print("progress is here : \(progress)")
                    
                    
                })
                //To check and verify server error
                /*upload.responseString(completionHandler: { (response) in
                 print(response)
                 print (response.result)
                 })*/
                upload.responseJSON
                    { response in
                        
                        switch response.result {
                        case .success:
                            print("responce is :\(response)")
                            completion(response)
                             alamoFireManager.session.invalidateAndCancel()
                            break
                        case .failure(let error):
                            print(error)
                              print("responce error is :\(error)")
                            completion(response)
                             alamoFireManager.session.invalidateAndCancel()
                        }
                        
                        
                }
                
            case .failure(_):
                print(result)
                 alamoFireManager.session.invalidateAndCancel()
                // completion(responds)
            }
    }
       
    }
    
    
  
    
    
  
}
