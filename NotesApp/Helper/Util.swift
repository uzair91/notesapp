//
//  Model.swift
//  SQLDatabase
//
//  Created by Drashti on 6/7/16.
//  Copyright © 2016 Yatin. All rights reserved.
//


import UIKit

class Util: NSObject {
    
    class func getPath(fileName: String) -> String {
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent(fileName)

        print(fileURL)
        
        
        
        documentsURL.appendingPathComponent(fileName)
//        let filemManager = FileManager.default
//        do {
//
//            try filemManager.removeItem(at: fileURL as URL)
//            print("Database Deleted!")
//        } catch {
//            print("Error on Delete Database!!!")
//        }
        
        return fileURL.path
    }
    
//    func deleteDatabase(filePath : String)
//    {
//        let filemManager = FileManager.default
//        do {
//            let fileURL = NSURL(fileURLWithPath: filePath)
//            try filemManager.removeItem(at: fileURL as URL)
//            print("Database Deleted!")
//        } catch {
//            print("Error on Delete Database!!!")
//        }
//    }
    class func copyFile(fileName: NSString) {
        let dbPath: String = getPath(fileName: fileName as String)
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: dbPath) {
            
            let documentsURL = Bundle.main.resourceURL
             let fromPath = documentsURL!.appendingPathComponent(fileName as String)
            print(fromPath)
            var error : NSError?
            do {
                try fileManager.copyItem(atPath: fromPath.path, toPath: dbPath)
            } catch let error1 as NSError {
                error = error1
            }
            let alert: UIAlertView = UIAlertView()
            if (error != nil) {
                alert.title = "Error Occured"
                alert.message = error?.localizedDescription
            } else {
                alert.title = "Successfully Copy"
                alert.message = "Your database copy successfully"
            }
            alert.delegate = nil
            alert.addButton(withTitle: "Ok")
            //alert.show()
        }
        else
        {
            
        }
    }
    
    
    func deleteDatabase(filePath : String)
    {
        let filemManager = FileManager.default
        do {
            let fileURL = NSURL(fileURLWithPath: filePath)
            try filemManager.removeItem(at: fileURL as URL)
            print("Database Deleted!")
        } catch {
            print("Error on Delete Database!!!")
        }
    }
    class func invokeAlertMethod(strTitle: NSString, strBody: NSString, delegate: AnyObject?) {
        let alert: UIAlertView = UIAlertView()
        alert.message = strBody as String
        alert.title = strTitle as String
        alert.delegate = delegate
        alert.addButton(withTitle: "Ok")
        alert.show()
    }
   
}
