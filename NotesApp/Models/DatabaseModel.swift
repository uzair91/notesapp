//
//  DatabaseModel.swift
//  NotesApp
//
//  Created by Basit on 4/26/19.
//  Copyright © 2019 APP. All rights reserved.
//

import Foundation

class LectureModel {
    
    var id: Int = 0
    var title: String = ""
    var category: String = ""
    var date: String = ""
    
    var decsricption: String = ""
    var bookmark: String = ""
    var name: String = ""
    var filepath: String = ""
    var length: String = ""
    var timestart: String = ""
    var timeend: String = ""
    var isDownaloded: Int = 0
    
    var processID: String = ""
    
    var lang: String = ""
    var isConverted: Int = 0
    
    
    
    init(){
        
    }
    
    func settimestart(timestart : String)
    {
        self.timestart=timestart
    }
    
    func gettimestart()->String
    {
        
        return self.timestart
    }
    
    func settimeend(timeend : String)
    {
        self.timeend = timeend
    }
    
    func gettimeend()->String
    {
        
        return self.timeend
    }
    
    
    func setlength(length : String)
    {
        self.length=length
    }
    
    func getlength()->String
    {
        
        return self.length
    }
    
    func setisDownaloded(isDownaloded : Int)
    {
        self.isDownaloded=isDownaloded
    }
    
    func getisDownaloded()->Int
    {
        
        return self.isDownaloded
    }
    
    
    func setisConverted(isConverted : Int)
    {
        self.isConverted=isConverted
    }
    
    func getisConverted()->Int
    {
        
        return self.isConverted
    }
    
    func setlang(lang : String)
    {
        self.lang=lang
    }
    
    func getlang()->String
    {
        
        return self.lang
    }
    
    
    
    
    
    
    func setid(id : Int)
    {
        self.id=id
    }
    
    func getid()->Int
    {
        
        return self.id
    }
    
    func settitle(title : String)
    {
        self.title=title
    }
    
    func gettitle()->String
    {
        
        return self.title
    }
    
    func setcategory(category : String)
    {
        self.category=category
    }
    
    func getcategory()->String
    {
        
        return self.category
    }
    
    func setdate(date : String)
    {
        self.date=date
    }
    
    func getdate()->String
    {
        
        return self.date
    }
    
    
    
    
    func setdecsricption(decsricption : String)
    {
        self.decsricption=decsricption
    }
    
    func getdecsricption()->String
    {
        
        return self.decsricption
    }
    
    
    func setbookmark(bookmark : String)
    {
        self.bookmark=bookmark
    }
    
    func getbookmark()->String
    {
        
        return self.bookmark
    }
    
    
    func setname(name : String)
    {
        self.name=name
    }
    
    func getname()->String
    {
        
        return self.name
    }
    
    func setfilepath(filepath : String)
    {
        self.filepath=filepath
    }
    
    func getfilepath()->String
    {
        
        return self.filepath
    }
    
    func setprocessID(processID : String)
    {
        self.processID=processID
    }
    
    func getprocessID()->String
    {
        
        return self.processID
    }
    
    
    
}
