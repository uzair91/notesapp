//
//  Protocol.swift
//  
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit

protocol ReusableView: class {}

extension ReusableView where Self: UIView {
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

protocol NibLoadableView: class { }

extension NibLoadableView where Self: UIView {
    
    static var NibName: String {
        return String(describing: self)
    }
}

protocol ShowsAlert {}
extension ShowsAlert where Self: UIViewController {

    func showLoader(){
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        let  loaderView = Bundle.loadView(fromNib:  ProgressHud.NibName, withType: ProgressHud.self)
        loaderView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(loaderView)
        loaderView.tag = 10008
        loaderView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        loaderView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        loaderView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0.0).isActive = true
        loaderView.topAnchor.constraint(equalTo: view.topAnchor, constant: 64.0).isActive = true
        
    }
    
    func hideLoader(){
        UIApplication.shared.endIgnoringInteractionEvents()
        let loaderView = self.view.viewWithTag(10008)
        loaderView?.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    func showAlertMessage(title: String , message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertMessageAndDisappear(title:String, message:String,timeDuration:Int,completionHandler:@escaping (Bool) -> ()) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        present(alertController, animated: true, completion: nil)
        alertController.view.backgroundColor = .gray
        // change to desired number of seconds (in this case 3 seconds)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alertController.dismiss(animated: true, completion: nil)
            completionHandler(true)
        }
    }
}
