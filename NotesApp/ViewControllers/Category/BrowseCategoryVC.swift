//
//  BrowseCategory.swift
//  NotesApp
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//
import UIKit

class BrowseCategoryVC: UIViewController , CellDelegate1  {
    
    @IBAction func deleteAction(_ sender: Any) {
        
            SharedHelper().showToast(message: "Please swipe right to delete category", controller: self)
    
        
    }
    var flag  = 0
    
    func didPressButtonEdit(_ tag: Int) {
        
        
         print("uzair hereeeee edit")
                     let view = EditCateogryPopup.instanceFromNib()
                                    view?.animator = FromTopTranslationAnimator()
            
                                    var l = CatgegoryModel()
                                    l = self.GetAllDataInfo.object(at: tag) as! CatgegoryModel
            
                                    view!.id = l.id!
                                    view!.title = l.cat_name
            
                                    view?.categoryTitle.text = l.cat_name
                                    view?.show()
        
//        addToolBar(textField: view!.categoryTitle)
       
                                    view?.yourobj={
                                        print("row number \(l.id!)")
            
            
            
            
            
            
                                        SharedHelper().showToast(message: "Edit category successfully", controller: self)
                                        //                self.tableView.reloadData()
                                        self.viewWillAppear(true)
                                        
                                        
                 }
        

    }
    
    func didPressButtondelete(_ tag: Int) {
        
        flag = 1
        
        
        print("uzair hereeeee")
        
    }
    
    
    
    @IBOutlet weak var tableView: UITableView!
     var GetAllDataInfo  = NSMutableArray()
    var cat_model_array = [CatgegoryModel]()
    var cat_mdeol : CatgegoryModel?
    
   
    
    @IBAction func addCategory(_ sender: Any) {
        
        
        let view = CreateNewCategoryPopUp.instanceFromNib()
        view?.animator = FromTopTranslationAnimator()
        view?.show()
//'
       
        view!.yourobj =
            {
                   SharedHelper().showToast(message: "Add category successfully", controller: self)
//                self.tableView.reloadData()
                self.viewWillAppear(true)
        }
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cat_mdeol = CatgegoryModel()
        cat_mdeol?.setcat_name(cat_name: "Assorted")
        
        cat_model_array.append(cat_mdeol!)
        
        
        
        
        
       
        
        
        tableView.register(UINib(nibName: CategoryTVC.NibName , bundle: nil), forCellReuseIdentifier: CategoryTVC.reuseIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GetAllDataInfo = FMDBDatabaseModel.getInstance().GetAllDataCateogry()
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addBtnTapped(_ sender: UIButton) {
        let view = CreateNewCategoryPopUp.instanceFromNib()
        view?.animator = FromTopTranslationAnimator()
    view?.show()
    }
}
extension BrowseCategoryVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GetAllDataInfo.count+1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CategoryTVC.reuseIdentifier, for: indexPath) as! CategoryTVC
        cell.selectionStyle = .none
//    let indexPath1 = self.tableView.indexPathForSelectedRow
        cell.cellDelegate = self
        cell.edtbtn.tag = indexPath.row - 1
//         cell.deletbtn.tag = indexPath.row - 1
        
//        cell.edtbtn.addTarget(cell, action: #selector(cell.categoryBtn(_:)), for: .touchUpInside)
//
//        cell.deletbtn.addTarget(cell, action:#selector(cell.deletebtaction(_:)), for: .touchUpInside)
        
       
        if(indexPath.row==0)
        {
//            cell.edtbtn.ishidden = true
            cell.edtbtn.isHidden = true
//            cell.deletbtn.isHidden = true
            
            
            cell.cat_name.text = self.cat_model_array[indexPath.row].cat_name
            
           
        }
        
        else
        {
           
            
            var l = CatgegoryModel()
            l = GetAllDataInfo.object(at: indexPath.row-1) as! CatgegoryModel
            
            cell.cat_name.text = l.cat_name
        }
        
  
      
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
     func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
//        let more = UITableViewRowAction(style: .normal, title: "More") { action, index in
//            print("more button tapped")
//        }
//        more.backgroundColor = .lightGray
//
//        let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { action, index in
//            print("favorite button tapped")
//        }
//        favorite.backgroundColor = .orange
//
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
        
            
//            print("index path is given here : \(editActionsForRowAt.row - 1)")
            
            
            if(editActionsForRowAt.row - 1 != -1)
            {
                var l = CatgegoryModel()
                l = self.GetAllDataInfo.object(at: editActionsForRowAt.row - 1) as! CatgegoryModel
                
                var refreshAlert = UIAlertController(title: "Aleart", message: "Do you really want to delete this category?", preferredStyle: UIAlertControllerStyle.alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    //                    self.deleteRecord()
                    
                    FMDBDatabaseModel.getInstance().deleteRecordFromCateogry(pid: l.id!)
                    SharedHelper().showToast(message: "Delete category successfully", controller: self)
                    //                self.tableView.reloadData()
                    self.viewWillAppear(true)
                    print("Handle Ok logic here")
                }))
                
                refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                    print("Handle Cancel Logic here")
                }))
                
                self.present(refreshAlert, animated: true, completion: nil)
            }
            else
            {
                 SharedHelper().showToast(message: "You can not delete or edit this cateogry", controller: self)
            }
            
            
        
        
        }
        delete.backgroundColor = .red
        
        return [delete]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = UIStoryboard.categoryStoryboard().instantiateViewController(withIdentifier: CategoryStoryBoards.CategoryName.rawValue) as! CategoryNameVC
//        self.navigationController?.pushViewController(vc, animated: true)

        
//       cell.btnView.tag = indexPath.row
        
//        let view = CreateNewCategoryPopUp.instanceFromNib()
//        view?.animator = FromTopTranslationAnimator()
//        view?.show()
//        view!.yourobj =
//            {
//                SharedHelper().showToast(message: "Add category successfully", controller: self)
//                //                self.tableView.reloadData()
//                self.viewWillAppear(true)
//        }
        
        

    }
}
