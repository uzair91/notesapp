//
//  PremiumPlanVC.swift
//  NotesApp
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit

class PremiumPlanVC: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

         collectionView.register(UINib(nibName: PremiumPlansCVC.NibName, bundle: nil), forCellWithReuseIdentifier: PremiumPlansCVC.reuseIdentifier)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension PremiumPlanVC: UICollectionViewDelegate,UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PremiumPlansCVC.reuseIdentifier, for: indexPath) as! PremiumPlansCVC
        
        return cell
    }
    
}
extension PremiumPlanVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
//        if HomeFeedarr.count == 0 {
//            let width = (collectionView.frame.size.width)
        
//            return CGSize(width:  width , height: height)
//        }
        
        //let height = (collectionView.frame.size.height/3)
        let width = (collectionView.frame.size.width/2)-10
        
        return CGSize(width:  width , height: width)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //top, left, bottom, right
        return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
    }
}
