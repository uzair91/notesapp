//
//  BookmarkLecturesVC.swift
//  NotesApp
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit

class BookmarkLecturesVC: UIViewController {
 
    @IBAction func deleteAction(_ sender: Any) {
        
          SharedHelper().showToast(message: "Please swipe right to delete bookmark", controller: self)
        
    }
    @IBOutlet weak var tableView: UITableView!
    var GetAllDataInfo  = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: LecturesBookmarkTVC.NibName , bundle: nil), forCellReuseIdentifier: LecturesBookmarkTVC.reuseIdentifier)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        GetAllDataInfo = FMDBDatabaseModel.getInstance().GetAllDatabookmark()
        tableView.reloadData()
    }
    
    @IBAction func trashIconTapped(_ sender: UIButton) {
        
    }
    
}

extension BookmarkLecturesVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GetAllDataInfo.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LecturesBookmarkTVC.reuseIdentifier, for: indexPath) as! LecturesBookmarkTVC
       
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        
        let currentDateTime = Date()
        
        // get the user's calendar
        let userCalendar = Calendar.current
        let requestedComponents: Set<Calendar.Component> = [
            .year,
            .month,
            .day,
            .hour,
            .minute,
            .second
        ]
        
        // get the components
        let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: currentDateTime)
        
        var l = LectureModel()
        l = GetAllDataInfo.object(at: indexPath.row) as! LectureModel
        cell.lecturecateory.text = l.category
//        cell.time.text = l.date
        cell.lecturetitle.text = l.title
//        cell.duration.text = l.length + "seconds"
       
        cell.date.text = String(getDayOfWeek(l.date)!)
        
        print("day by date \(getDayOfWeek(l.date)!)")
        print("month by date \(getMonthOfWeek(l.date)!)")
        //        description1 = l.decsricption
        
        cell.month.text = String(getMonthOfWeek(l.date)!)
        
//         cell.day.text = String(getDayOfWeek(l.date)!)
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: MainStoryBoard.LectureDetails.rawValue) as! LectureDetailsVC
        var l = LectureModel()
        l = GetAllDataInfo.object(at: indexPath.row) as! LectureModel
     
        print("pdi \(String(l.processID))")
        
        print("l.title \(String(l.title))")
        
        print("l.date \(String(l.date))")
        
        print("l.filepath \(String(l.filepath))")
        
        print("l.filepath \(String(l.filepath))")
        
        vc.pid = String(l.processID)
        
        
        vc.strCat = l.title
        vc.strDateTime = l.date
        vc.strTime = l.length + " seconds"
        vc.filepath1 = l.filepath
        vc.refText = l.decsricption
        
         vc.duration = l.length
        
        vc.id = 2
        vc.lectureModel = l
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        //        formatter.dateFormat = "yyyy-MM-dd"
        
        formatter.dateFormat  = "ddLLLL,yyyy"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.day, from: todayDate)
        return weekDay
    }
    

    
    func getMonthOfWeek(_ today:String) -> String? {
        let formatter  = DateFormatter()
        //        formatter.dateFormat = "yyyy-MM-dd"
        
        formatter.dateFormat  = "ddLLLL,yyyy"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.month, from: todayDate)
        
        let monthName = DateFormatter().monthSymbols[weekDay - 1]
        
        return monthName
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        //        let more = UITableViewRowAction(style: .normal, title: "More") { action, index in
        //            print("more button tapped")
        //        }
        //        more.backgroundColor = .lightGray
        //
        //        let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { action, index in
        //            print("favorite button tapped")
        //        }
        //        favorite.backgroundColor = .orange
        //
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            
            
            //            print("index path is given here : \(editActionsForRowAt.row - 1)")
            
            
           
                var l = LectureModel()
                l = self.GetAllDataInfo.object(at: editActionsForRowAt.row) as! LectureModel
            
            
            
                var refreshAlert = UIAlertController(title: "Aleart", message: "Do you really want to delete this bookmark?", preferredStyle: UIAlertControllerStyle.alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    //                    self.deleteRecord()
//                    deleteRecordFromBookmark
                    FMDBDatabaseModel.getInstance().deleteRecordFromBookmark(pid: String(l.processID))
//                    FMDBDatabaseModel.getInstance().deleteRecordFromCateogry(pid: l.id!)
                    
//                    FMDBDatabaseModel.getInstance().delete
                    
                    
//                    print("l id is here \(l.id)")
                    
                    SharedHelper().showToast(message: "Delete Bookmark successfully", controller: self)
                    //                self.tableView.reloadData()
                    self.viewWillAppear(true)
                    print("Handle Ok logic here")
                }))
                
                refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                    print("Handle Cancel Logic here")
                }))
                
                self.present(refreshAlert, animated: true, completion: nil)
           
            
            
            
            
        }
        delete.backgroundColor = .red
        
        return [delete]
    }
}

//extension BookmarkLecturesVC : FSPagerViewDataSource, FSPagerViewDelegate {
//
//    public func numberOfItems(in pagerView: FSPagerView) -> Int {
//        return 25
//    }
//
//    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
//        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: LecturesBottomSliderView.reuseIdentifier, at: index) as! LecturesBottomSliderView
//        cell.itemNameLbl.text = "\(index+1)"
//        //        cell.delegate = self
//        return cell
//    }
//
//    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
//
//        pagerView.deselectItem(at: index, animated: true)
//        pagerView.scrollToItem(at: index, animated: true)
//    }
//
//}

