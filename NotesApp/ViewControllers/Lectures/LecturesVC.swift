//
//  LecturesVC.swift
//  NotesApp
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit
import FSPagerView




class LecturesVC: UIViewController  {
    
    @IBAction func deleteAction(_ sender: Any) {
        
         SharedHelper().showToast(message: "Please swipe right to delete lecture", controller: self)
    }
    @IBAction func searchAction(_ sender: Any) {
        
        
        
                let vc = UIStoryboard.lecturesStoryboard().instantiateViewController(withIdentifier: LecturesStoryBoard.SearchLecture.rawValue) as! SearchLecturesVC
                self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    var description1 : String = ""
    var curent : Int = 0
    var trimmedStringdate : String = ""
    var subtractDate : Int = 0
    var addDate : Int = 0
   
    
     static var  recording: Recording!
    
    @IBAction func leftarrowaction(_ sender: Any) {
       
        
       
        if(curent  > 0)
        {
         subtractDate = subtractDate - 1
             curent = curent - 1
            
            print("current is here\(curent)")
            
             self.pagerView.scrollToItem(at: curent, animated: true)
            
            let dateFormatter1 : DateFormatter = DateFormatter()
            //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter1.dateFormat = "dd MMM,yyyy"
            let date1 = Date()
            let dateString1 = dateFormatter1.string(from: date1)
            
//            print("current date\(dateString1)")
            
            
            
//            print("currentdayyyy \(Calendar.current.date(byAdding: .day, value: -(curent), to: date1)!)")
//
            
            let dateString12 = dateFormatter1.string(from: Calendar.current.date(byAdding: .day, value: subtractDate, to: date1)!)
            
            //        Calendar.current.date(from: DateComponents)
            //        Calendar.current
            
            trimmedStringdate = dateString12.replacingOccurrences(of: " ", with: "")
            print("nextt day \(trimmedStringdate)")
            
            GetAllDataInfo = FMDBDatabaseModel.getInstance().getLecturesFromDate(date: trimmedStringdate)
            tableView.reloadData()
            
        }
            
            
        
        
        
    }
    @IBOutlet weak var leftarrow: UIButton!
    @IBOutlet weak var rightarrow: UIButton!
    var length : Int = 0
    
    var currentday : Int = 0
    
    
    @IBOutlet weak var leftArrowAction: UIButton!
    @IBAction func rightArraowaction(_ sender: Any) {
//         print("next current \(curent)")
        if(curent + 1 < length  )
        {
            if(curent + 1 < currentday)
            
            {
                
                subtractDate = subtractDate + 1
                
              curent = curent + 1
                print("current is here\(curent)")
                
                self.pagerView.scrollToItem(at: curent, animated: true)
                
                let dateFormatter1 : DateFormatter = DateFormatter()
                //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                
                
                dateFormatter1.dateFormat = "dd MMM,yyyy"
                let date1 = Date()
                let dateString1 = dateFormatter1.string(from: date1)
                
                 print("datestring day \(dateString1)")
                
                //            print("currentdayyyy \(Calendar.current.date(byAdding: .day, value: curent, to: date1)!)")
                
                
                let dateString12 = dateFormatter1.string(from: Calendar.current.date(byAdding: .day, value: subtractDate, to: date1)!)
                
                //        Calendar.current.date(from: DateComponents)
                //        Calendar.current
//                myString.replacingOccurrences(of: " ", with: "")
                 trimmedStringdate = dateString12.replacingOccurrences(of: " ", with: "")
                print("nextt day \(trimmedStringdate)")
                GetAllDataInfo = FMDBDatabaseModel.getInstance().getLecturesFromDate(date: trimmedStringdate)
                tableView.reloadData()
               
            }
           
            
            
        }
        }
    
    @IBOutlet weak var cmonth: UILabel!
    @IBAction func deletebtn(_ sender: Any) {
        
        SharedHelper().showToast(message: "Please swipe right to delete lecture", controller: self)
        
    }
    @IBOutlet weak var tableView: UITableView!
     var GetAllDataInfo  = NSMutableArray()
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.isInfinite = false
            
            self.pagerView.transformer = FSPagerViewTransformer(type: .linear)
            
           
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        GetAllDataInfo = FMDBDatabaseModel.getInstance().GetAllData()
        
       
        
//        print("date string is here \(dateString1.replacingOccurrences(of: " ", with: ""))")
        
       
        GetAllDataInfo = FMDBDatabaseModel.getInstance().getLecturesFromDate(date:trimmedStringdate)
        
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableView.register(UINib(nibName: SearchLectureTVC.NibName , bundle: nil), forCellReuseIdentifier: SearchLectureTVC.reuseIdentifier)
        pagerView.register(UINib(nibName: LecturesBottomSliderView.NibName, bundle: nil), forCellWithReuseIdentifier: LecturesBottomSliderView.reuseIdentifier)
        DispatchQueue.main.async {
            self.pagerView.itemSize = self.pagerView.frame.size.applying(CGAffineTransform(scaleX: 0.2, y: 1))
        }
        
        
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        let nameOfMonth = dateFormatter.string(from: now)
        
        
        self.pagerView.isUserInteractionEnabled = false
        
        
        cmonth.text = nameOfMonth
        
        print("month current \(nameOfMonth)")
        
        let calendar = Calendar.current
        let date = Date()
        
          currentday = Calendar.current.component(.day, from: date)
        
        
        
          self.curent = currentday - 1
      
//        let calender1 : FSCalendar?
       
        let dateFormatter1 : DateFormatter = DateFormatter()
        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter1.dateFormat = "dd MMM,yyyy"
        let date1 = Date()
         let dateString1 = dateFormatter1.string(from: date1)
        
         trimmedStringdate = dateString1.replacingOccurrences(of: " ", with: "")
        GetAllDataInfo = FMDBDatabaseModel.getInstance().getLecturesFromDate(date:trimmedStringdate)
        
        tableView.reloadData()
//        // Calculate start and end of the current year (or month with `.month`):
//        let interval = calendar.dateInterval(of: .year, for: date)! //change year it will no of days in a year , change it to month it will give no of days in a current month
//
//        // Compute difference in days:
//        let days = calendar.dateComponents([.day], from: interval.start, to: interval.end).day!
//        print("days is here \(days)")
        
        
        
        
       
        let cal = NSCalendar(calendarIdentifier:NSCalendar.Identifier.gregorian)!
        let days = cal.range(of: .day, in: .month, for: date)
        
//       print("days is here \(days.length)")
        
        length =  days.length
        
        // Calculate start and end of the current year (or month with `.month`):
//        let interval = calendar.dateInterval(of: .year, for: date)!
        
        // Compute difference in days:
//        _ = NSDate()
//        let cal = NSCalendar(calendarIdentifier:NSCalendar.Identifier.gregorian)!
//        let days = cal.rangeOfUnit(.day, inUnit: .Month, forDate: date)
        
        
       
        // Calculate start and end of the current year (or month with `.month`):
        
        
        // Compute difference in days:
       
//        print(days)
        
        
     
       
        
       
        
    }
    
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd"
        
        formatter.dateFormat  = "ddLLLL,yyyy"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.day, from: todayDate)
        return weekDay
    }
    
    func getMonthOfWeek(_ today:String) -> String? {
        let formatter  = DateFormatter()
        //        formatter.dateFormat = "yyyy-MM-dd"
        
        formatter.dateFormat  = "ddLLLL,yyyy"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.month, from: todayDate)
        
        let monthName = DateFormatter().monthSymbols[weekDay - 1]
        
        return monthName
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func trashIconTapped(_ sender: UIButton) {
        
        
//        let vc = UIStoryboard.lecturesStoryboard().instantiateViewController(withIdentifier: LecturesStoryBoard.SearchLecture.rawValue) as! SearchLecturesVC
//        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
extension LecturesVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return GetAllDataInfo.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchLectureTVC.reuseIdentifier, for: indexPath) as! SearchLectureTVC
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        
        let currentDateTime = Date()
        
        // get the user's calendar
        let userCalendar = Calendar.current
        let requestedComponents: Set<Calendar.Component> = [
            .year,
            .month,
            .day,
            .hour,
            .minute,
            .second
        ]
        
        // get the components
        let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: currentDateTime)
        
        var l = LectureModel()
        l = GetAllDataInfo.object(at: indexPath.row) as! LectureModel
        cell.category.text = l.category
        cell.time.text = l.date
        cell.title.text = l.title
        cell.duration.text = l.length + "seconds"
        cell.day.text = String(getDayOfWeek(l.date)!)
        
        print("day by date \(getDayOfWeek(l.date)!)")
        print("month by date \(getMonthOfWeek(l.date)!)")
//        description1 = l.decsricption
       
        cell.month.text = getMonthOfWeek(l.date)!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: MainStoryBoard.LectureDetails.rawValue) as! LectureDetailsVC
        
        var l = LectureModel()
        l = GetAllDataInfo.object(at: indexPath.row) as! LectureModel
//        cell.category.text = l.category
//        cell.time.text = l.date
//        cell.title.text = l.title
//        cell.duration.text = l.length + "seconds"
//        cell.day.text = String(describing: dateTimeComponents.day!)
//
//        cell.month.text = String(describing: dateTimeComponents.month!)
//
        
        print("pdi \(String(l.processID))")
        
        print("l.title \(String(l.title))")
        
        print("l.date \(String(l.date))")
        
        print("l.filepath \(String(l.filepath))")
        
      
        
        vc.pid = String(l.processID)
        
        
        vc.strCat = l.title
        vc.strDateTime = l.date
        vc.strTime = l.length + " seconds"
        vc.filepath1 = l.filepath
        vc.refText = l.decsricption
        
        
        vc.id = 1
        vc.lectureModel = l
        
        vc.duration = l.length
        
//        vc.pid = String(PID)
        
        //
        //                        lebLtime1.text = strTime
        //                        lbldateTime.text = strDateTime
        
       
//        vc.pid = String(PID)
//
//
//        vc.strCat = self.txtTitle.text!
//        vc.strDateTime = self.txtDate.text!
//        vc.strTime = self.txtTime.text!
//        vc.filepath = self.filepath
//
        
//        self.saveRecordDb(pid: String(PID))

        
//        vc.pid="534"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        //        let more = UITableViewRowAction(style: .normal, title: "More") { action, index in
        //            print("more button tapped")
        //        }
        //        more.backgroundColor = .lightGray
        //
        //        let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { action, index in
        //            print("favorite button tapped")
        //        }
        //        favorite.backgroundColor = .orange
        //
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            
            
            //            print("index path is given here : \(editActionsForRowAt.row - 1)")
            
            
            
            var l = LectureModel()
            l = self.GetAllDataInfo.object(at: editActionsForRowAt.row) as! LectureModel
            
            
            
            var refreshAlert = UIAlertController(title: "Aleart", message: "Do you really want to delete this lecture?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                //                    self.deleteRecord()
                //                    deleteRecordFromBookmark
//                FMDBDatabaseModel.getInstance().deleteRecordFromBookmark(pid: l.id)
                
                print("")
                
                FMDBDatabaseModel.getInstance().deleteRecordFromRecording(pid: String(l.processID))
                
                //                    FMDBDatabaseModel.getInstance().deleteRecordFromCateogry(pid: l.id!)
                
                //                    FMDBDatabaseModel.getInstance().delete
                
                
                //                    print("l id is here \(l.id)")
                
                SharedHelper().showToast(message: "Delete Lecture successfully", controller: self)
                //                self.tableView.reloadData()
                self.viewWillAppear(true)
                print("Handle Ok logic here")
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            self.present(refreshAlert, animated: true, completion: nil)
            
            
            
            
            
        }
        delete.backgroundColor = .red
        
        return [delete]
    }
}

// MARK:- FSPagerViewDataSource and FSPagerViewDelegate Methods
extension LecturesVC : FSPagerViewDataSource, FSPagerViewDelegate {
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return length
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: LecturesBottomSliderView.reuseIdentifier, at: index) as! LecturesBottomSliderView
        cell.itemNameLbl.text = "\(index+1)"
        
        
        
       
        
        
        
        if(index+1 == currentday)
        {
            
            cell.backgroundColor = UIColor.orange
            cell.tintColor = UIColor.orange
            
//            pagerView.index(for: cell)
            
           
        }
        else
        {
            cell.backgroundColor = UIColor.lightGray
            cell.tintColor = UIColor.lightGray
        }
        
          self.pagerView.scrollToItem(at: curent, animated: true)
        
        
        
        
//        if(index+1 == currentday-1)
//        {
//            cell.backgroundColor = UIColor.red
//
//        }
       
//         self.pagerView.scrollToItem(at: currentday-1, animated: false)
//        self.pagerView.currentIndex = self.currentday
        //        cell.delegate = self
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        
        
        pagerView.deselectItem(at: currentday, animated: true)
//        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
//       print("current date  \(self.pagerView.currentIndex)")
//         print("current date  \(targetIndex)")
        
        print("current date  \(targetIndex)")
        
        
       
    }
   

    
}

