//
//  SearchLecturesVC.swift
//  NotesApp
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit

class SearchLecturesVC: UIViewController {

    @IBAction func searchAction(_ sender: Any) {
        
        if(searchedittext.text=="")
        {
            SharedHelper().showToast(message: "Please insert text for searching", controller: self)
        }
        else
        {
            GetAllDataInfo = FMDBDatabaseModel.getInstance().getLecturesFromTitle(title: searchedittext.text!)
            
            //        let dateFormatter1 : DateFormatter = DateFormatter()
            //        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            //        dateFormatter1.dateFormat = "dd MMM,yyyy"
            //        let date1 = Date()
            //        let dateString1 = dateFormatter1.string(from: date1)
            
            
            
            //        GetAllDataInfo = FMDBDatabaseModel.getInstance().getLecturesFromDate(date: dateString1.replacingOccurrences(of: " ", with: ""))
            
            tableView.reloadData()
        }
        
       
        
    }
    @IBOutlet weak var totalRecords: UILabel!
    @IBOutlet weak var searchactionbutton: UIButton!
    @IBOutlet weak var searchedittext: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var GetAllDataInfo  = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        searchedittext.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.EditingChanged)
        
      
        self.searchedittext.delegate = self
        
//        searchedittext.returnKeyType = UIReturnKeyType;
        searchedittext.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        
        
        tableView.register(UINib(nibName: SearchLectureTVC.NibName , bundle: nil), forCellReuseIdentifier: SearchLectureTVC.reuseIdentifier)
        
        addToolBar(textField: searchedittext)
        
          NotificationCenter.default.addObserver(self, selector: #selector(self.doneclicked(notification:)), name: Notification.Name("donePressed"), object: nil)
        
    }
    
    @objc private func doneclicked(notification: NSNotification){
        //do stuff using the userInfo property of the notification object
        
        if(searchedittext.text=="")
        {
            SharedHelper().showToast(message: "Please insert text for searching", controller: self)
        }
        else
        {
            GetAllDataInfo = FMDBDatabaseModel.getInstance().getLecturesFromTitle(title: searchedittext.text!)
            
            //        let dateFormatter1 : DateFormatter = DateFormatter()
            //        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            //        dateFormatter1.dateFormat = "dd MMM,yyyy"
            //        let date1 = Date()
            //        let dateString1 = dateFormatter1.string(from: date1)
            
            
            
            //        GetAllDataInfo = FMDBDatabaseModel.getInstance().getLecturesFromDate(date: dateString1.replacingOccurrences(of: " ", with: ""))
            
            tableView.reloadData()
        }
        
        
    }
    
   
    
    @objc func doneButtonClicked(_ sender: Any) {
        //your code when clicked on done
        
    
        
     
        
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        
        textField.resignFirstResponder()
        print("keybaord action \(textField.text)")
        return true
    }
    
//    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
////        self.updateCharacterCount()
//
//        print("counter is here \(textView.text.characters.count +  (text.characters.count - range.length))")
//
//        return textView.text.characters.count +  (text.characters.count - range.length) <= 65
//    }
    
    func textFieldDidChange(textField : UITextField){
        print("counter is here \(textField.text?.characters.count)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
                GetAllDataInfo = FMDBDatabaseModel.getInstance().GetAllData()
        
//        let dateFormatter1 : DateFormatter = DateFormatter()
//        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        dateFormatter1.dateFormat = "dd MMM,yyyy"
//        let date1 = Date()
//        let dateString1 = dateFormatter1.string(from: date1)
        
        
        
//        GetAllDataInfo = FMDBDatabaseModel.getInstance().getLecturesFromDate(date: dateString1.replacingOccurrences(of: " ", with: ""))
        
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func searchBtnTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
    
}





extension SearchLecturesVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        self.totalRecords.text = "\(GetAllDataInfo.count) records found "
        
        return GetAllDataInfo.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchLectureTVC.reuseIdentifier, for: indexPath) as! SearchLectureTVC
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        
        let currentDateTime = Date()
        
        // get the user's calendar
        let userCalendar = Calendar.current
        let requestedComponents: Set<Calendar.Component> = [
            .year,
            .month,
            .day,
            .hour,
            .minute,
            .second
        ]
        
        // get the components
        let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: currentDateTime)
        
        var l = LectureModel()
        l = GetAllDataInfo.object(at: indexPath.row) as! LectureModel
        cell.category.text = l.category
        cell.time.text = l.date
        cell.title.text = l.title
        cell.duration.text = l.length + "seconds"
        cell.day.text = String(getDayOfWeek(l.date)!)
        
        print("day by date \(getDayOfWeek(l.date)!)")
        print("month by date \(getMonthOfWeek(l.date)!)")
        //        description1 = l.decsricption
        
        cell.month.text = String(getMonthOfWeek(l.date)!)
        
        return cell
    }
    
    
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        //        formatter.dateFormat = "yyyy-MM-dd"
        
        formatter.dateFormat  = "ddLLLL,yyyy"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.day, from: todayDate)
        return weekDay
    }
    
    func getMonthOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        //        formatter.dateFormat = "yyyy-MM-dd"
        
        formatter.dateFormat  = "ddLLLL,yyyy"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.month, from: todayDate)
        return weekDay
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: MainStoryBoard.LectureDetails.rawValue) as! LectureDetailsVC
        
        var l = LectureModel()
        l = GetAllDataInfo.object(at: indexPath.row) as! LectureModel
        //        cell.category.text = l.category
        //        cell.time.text = l.date
        //        cell.title.text = l.title
        //        cell.duration.text = l.length + "seconds"
        //        cell.day.text = String(describing: dateTimeComponents.day!)
        //
        //        cell.month.text = String(describing: dateTimeComponents.month!)
        //
        
        print("pdi \(String(l.processID))")
        
        print("l.title \(String(l.title))")
        
        print("l.date \(String(l.date))")
        
        print("l.filepath \(String(l.filepath))")
        
        
        
        vc.pid = String(l.processID)
        
        
        vc.strCat = l.title
        vc.strDateTime = l.date
        vc.strTime = l.length + " seconds"
        vc.filepath1 = l.filepath
        vc.refText = l.decsricption
        
        
        
        vc.id = 1
        vc.lectureModel = l
        
        vc.duration = l.length
        //        vc.pid = String(PID)
        //
        //
        //        vc.strCat = self.txtTitle.text!
        //        vc.strDateTime = self.txtDate.text!
        //        vc.strTime = self.txtTime.text!
        //        vc.filepath = self.filepath
        //
        
        //        self.saveRecordDb(pid: String(PID))
        
        
        //        vc.pid="534"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        //        let more = UITableViewRowAction(style: .normal, title: "More") { action, index in
        //            print("more button tapped")
        //        }
        //        more.backgroundColor = .lightGray
        //
        //        let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { action, index in
        //            print("favorite button tapped")
        //        }
        //        favorite.backgroundColor = .orange
        //
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            
            
            //            print("index path is given here : \(editActionsForRowAt.row - 1)")
            
            
            
            var l = LectureModel()
            l = self.GetAllDataInfo.object(at: editActionsForRowAt.row) as! LectureModel
            
            
            
            var refreshAlert = UIAlertController(title: "Aleart", message: "Do you really want to delete this lecture?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                //                    self.deleteRecord()
                //                    deleteRecordFromBookmark
                //                FMDBDatabaseModel.getInstance().deleteRecordFromBookmark(pid: l.id)
                
                print("")
                
                FMDBDatabaseModel.getInstance().deleteRecordFromRecording(pid: String(l.processID))
                
                //                    FMDBDatabaseModel.getInstance().deleteRecordFromCateogry(pid: l.id!)
                
                //                    FMDBDatabaseModel.getInstance().delete
                
                
                //                    print("l id is here \(l.id)")
                
                SharedHelper().showToast(message: "Delete Lecture successfully", controller: self)
                //                self.tableView.reloadData()
                self.viewWillAppear(true)
                self.searchedittext.text = ""
                print("Handle Ok logic here")
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            self.present(refreshAlert, animated: true, completion: nil)
            
            
            
            
            
        }
        delete.backgroundColor = .red
        
        return [delete]
    }
}

// MARK:- FSPagerViewDataSource and FSPagerViewDelegate Methods

