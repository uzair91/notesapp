//
//  LeftMenuVC.swift
//  

//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit
import CoreTelephony
class LeftMenuVC: UIViewController {
    
    //MARK:- My IBOutlets
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var topView: UIView!
    
    static let startLecture = "Start Lecture"
    static let lectures = "Lectures"
    static let browserCategories = "Browse Categories"
    static let bookmarks = "Bookmarks"
    static let settings = "Settings"
    static let sendFeedback = "Send Feedback"
    static let shareApp = "Share App"
    static let rateApp = "Rate App"
    static let upgradetoPremium  = "Upgrade to Premium"
    static let privacy = "Privacy Policy / T&C"
    static let about = "About"
    static let chats = "Chats"
    
    //MARK:- My Helper Variables
    let titlesArray = [startLecture,
                       lectures,
                       browserCategories,
                       bookmarks,
                       chats,
                       about,
                       shareApp,
                       rateApp,
                       upgradetoPremium
                    ]
    
    let imageArray = [#imageLiteral(resourceName: "Group 136"),#imageLiteral(resourceName: "Group 136"),#imageLiteral(resourceName: "Path 53"),#imageLiteral(resourceName: "Group 80"),#imageLiteral(resourceName: "Path 52"),#imageLiteral(resourceName: "Group 138"),#imageLiteral(resourceName: "Group 135"),#imageLiteral(resourceName: "Group 134"),#imageLiteral(resourceName: "Path 47")]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        menuTableView.register(UINib(nibName: LeftMenuTVC.NibName , bundle: nil), forCellReuseIdentifier: LeftMenuTVC.reuseIdentifier)
        menuTableView.backgroundColor = .clear
        topView.roundCorners([.bottomRight], radius: 10.0)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(cameBackFromSleep),
            name: NSNotification.Name.UIApplicationDidBecomeActive,
            object: nil
        )
    }
    
    private func isOnPhoneCall() -> Bool
    {
        let callCntr = CTCallCenter()
        
        if let calls = callCntr.currentCalls
        {
            for call in calls
            {
                if call.callState == CTCallStateConnected || call.callState == CTCallStateDialing || call.callState == CTCallStateIncoming
                {
                    print("In call")
                    return true
                }
            }
        }
        
        print("No calls")
        return false
    }
    
    @objc func cameBackFromSleep()
    {
        
        self.viewWillAppear(true)
    }
    
    //MARK:- My Helper Methods
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .fade
    }
    
    //MARK:- My Deinit Method
    deinit {
        debugPrint("LeftMenuVC deinit called")
    }
}

//MARK:- My TableView Methods Extension
extension LeftMenuVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titlesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.menuTableView.dequeueReusableCell(withIdentifier: LeftMenuTVC.reuseIdentifier, for: indexPath) as! LeftMenuTVC
        cell.itemLbl.text = titlesArray[indexPath.row]
        cell.itemImage.image = imageArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if( RecordLectureVC.image == #imageLiteral(resourceName: "Group 394"))
        {
            var refreshAlert = UIAlertController(title: "Alert", message: "Please stop recording first", preferredStyle: UIAlertControllerStyle.alert)
            
//            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
//                self.deleteRecord()
//                print("Handle Ok logic here")
//            }))
//
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(refreshAlert, animated: true, completion: nil)
            sideMenuController?.hideLeftView(animated: true, completionHandler: { })
           
            
            
        }
      
        
       
        else
        {
            
            
            var navController : UINavigationController?
            for vc in (self.sideMenuController?.childViewControllers)! {
                if vc is UINavigationController {
                    navController = vc as? UINavigationController
                    break
                }
            }
            
            sideMenuController?.hideLeftView(animated: true, completionHandler: { })
            
            if titlesArray[indexPath.row] == LeftMenuVC.startLecture {
                guard navController?.viewControllers.last is RecordLectureVC else {
                    
                    
                    let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: MainStoryBoard.RecordLecture.rawValue) as! RecordLectureVC
                    navController?.pushViewController(vc, animated: true)
                    return
                }
            }else if titlesArray[indexPath.row] == LeftMenuVC.lectures {
                guard navController?.viewControllers.last is LecturesVC else {
                    let vc = UIStoryboard.lecturesStoryboard().instantiateViewController(withIdentifier: LecturesStoryBoard.Lectures.rawValue) as! LecturesVC
                    navController?.pushViewController(vc, animated: true)
                    return
                }
                
            }else if titlesArray[indexPath.row] == LeftMenuVC.browserCategories {
                guard navController?.viewControllers.last is BrowseCategoryVC else {
                    let vc = UIStoryboard.categoryStoryboard().instantiateViewController(withIdentifier: CategoryStoryBoards.BrowseCategory.rawValue) as! BrowseCategoryVC
                    navController?.pushViewController(vc, animated: true)
                    return
                }
            }else if titlesArray[indexPath.row] == LeftMenuVC.bookmarks {
                guard navController?.viewControllers.last is BookmarkLecturesVC else {
                    let vc = UIStoryboard.lecturesStoryboard().instantiateViewController(withIdentifier: LecturesStoryBoard.BookmarkLectures.rawValue) as! BookmarkLecturesVC
                    navController?.pushViewController(vc, animated: true)
                    return
                }
            }else if titlesArray[indexPath.row] == LeftMenuVC.settings {
                
            }else if titlesArray[indexPath.row] == LeftMenuVC.sendFeedback {
                guard navController?.viewControllers.last is FeedbackVC else {
                    let vc = UIStoryboard.settingsStoryboard().instantiateViewController(withIdentifier: SettingsStoryBoards.Feedback.rawValue) as! FeedbackVC
                    navController?.pushViewController(vc, animated: true)
                    return
                }
            }else if titlesArray[indexPath.row] == LeftMenuVC.shareApp  ||  titlesArray[indexPath.row] == LeftMenuVC.rateApp{
                
            }else if titlesArray[indexPath.row] == LeftMenuVC.upgradetoPremium {
                guard navController?.viewControllers.last is PremiumPlanVC else {
                    let vc = UIStoryboard.categoryStoryboard().instantiateViewController(withIdentifier: CategoryStoryBoards.PremiumPlan.rawValue) as! PremiumPlanVC
                    navController?.pushViewController(vc, animated: true)
                    return
                }
            }else if titlesArray[indexPath.row] == LeftMenuVC.privacy {
                
            }else if titlesArray[indexPath.row] == LeftMenuVC.about {
                guard navController?.viewControllers.last is About_Us else {
                    let vc = UIStoryboard.settingsStoryboard().instantiateViewController(withIdentifier: SettingsStoryBoards.About.rawValue) as! About_Us
                    navController?.pushViewController(vc, animated: true)
                    return
                }
            }
        }
        
       
    }
}


