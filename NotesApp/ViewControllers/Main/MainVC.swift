//
//  MainVC.swift
//  
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit
import LGSideMenuController

class MainVC: LGSideMenuController {
    
    var regularStyle: UIBlurEffectStyle? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupLeftAndRootView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- My Helper Methods
    func setupLeftAndRootView() {
        if #available(iOS 10.0, *) {
            regularStyle = .regular
        }
        else {
            regularStyle = .light
        }
        leftViewWidth = 280.0
        leftViewBackgroundColor = .clear
        rootViewCoverColorForLeftView = .clear
        leftViewLayerShadowRadius = 100.0
        leftViewPresentationStyle = .slideAbove
        leftViewLayerShadowColor = UIColor.white
        isLeftViewSwipeGestureEnabled = false
    }
    
    override func leftViewWillLayoutSubviews(with size: CGSize) {
        super.leftViewWillLayoutSubviews(with: size)
        
        if !isLeftViewStatusBarHidden {
            leftView?.frame = CGRect(x: 0.0, y: 20.0, width: size.width, height: size.height - 20.0)
        }
    }
    
    //MARK:- My Deinit Method
    deinit {
        debugPrint("MainVC deinit called")
    }
}


