//
//  ReferenceFullTextController.swift
//  NotesApp
//
//  Created by Basit on 5/20/19.
//  Copyright © 2019 APP. All rights reserved.
//

import UIKit

class ReferenceFullTextController: UIViewController, UIWebViewDelegate {

    
    @IBAction func backBtn(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    var url : URL?
    @IBOutlet weak var WEBVIEW: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
      
        WEBVIEW.loadRequest(URLRequest(url:url!))
        
        
        
        WEBVIEW.delegate=self
        
        // Do any additional setup after loading the view.
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
//        SharedHelper().showSpinner(view: self.view)
       
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
//        SharedHelper().hideSpinner(view: self.view)
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: NSError?) {
        
//        SharedHelper().hideSpinner(view: self.view)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
