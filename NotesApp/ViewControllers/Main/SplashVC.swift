//
//  SplashVC.swift
//  NotesApp

//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit




class SplashVC: UIViewController {
public static var path : URL?
    public static var pathString : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let db = DatabaseManager()
        
        SplashVC.path  = db.getUrl()
        
        print("splash path is \(SplashVC.path!)")
        
//        filePathUrl()
        
       
        
//        public var  COLUMN_NAME_LECTURE_TITLE = "lecture_title";
//        public var  COLUMN_NAME_LECTURE_CATEGORY = "lecture_category";
//        public var COLUMN_NAME_LECTURE_DATE = "lecture_date";
//        public var COLUMN_NAME_LECTURE_DESC = "lecture_desc";
//        public var COLUMN_NAME_LECTURE_BOOKMARK = "lecture_bookmark";
//        public var COLUMN_NAME_RECORDING_NAME = "recording_name";
//        public var COLUMN_NAME_RECORDING_FILE_PATH = "file_path";
//        public var COLUMN_NAME_RECORDING_LENGTH = "length";
//        public var COLUMN_NAME_TIME_START = "time_start";
//        public var COLUMN_NAME_TIME_END = "time_end";
//        public var COLUMN_NAME_PROCESS_ID = "pid";
//        public var COLUMN_NAME_IS_DOWNLOADED = "is_downloaded";

//
//        var lectureModel = LectureModel()
//        lectureModel.settitle(title: "title is uzair")
//        lectureModel.setcategory(category: "food")
//        lectureModel.setdate(date: "dateis1")
//        lectureModel.setdecsricption(decsricption: "description")
//        lectureModel.setbookmark(bookmark: "bookmark")
//        lectureModel.setname(name: "name is uzair")
//        lectureModel.setfilepath(filepath: "file path is here")
//        lectureModel.setlength(length: 2)
//        lectureModel.settimestart(timestart: 2.0)
//        lectureModel.settimeend(timeend: 3.0)
//        lectureModel.setprocessID(processID: "process id is here")
//        lectureModel.setisDownaloded(isDownaloded: 0)
//
//        FMDBDatabaseModel.getInstance().InsertData(lectureModel)
//        FMDBDatabaseModel.getInstance().getData()
        
        

        // Do any additional setup after loading the view.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.perform(#selector(navigateLogin), with: nil, afterDelay: 0.5)
    }
    
    
    //MARK:- My Helper Methods
    @objc func navigateLogin() {
//        guard Helper.getUser() == nil else{
//            let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: MainStoryBoard.MapRoute.rawValue) as! MapRouteVC
//            self.navigationController?.pushViewController(vc, animated: false)
//            return
//        }
        
        let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: MainStoryBoard.RecordLecture.rawValue) as! RecordLectureVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    //MARK:- My Deinit Method
    deinit {
        debugPrint("SplashVC deinit called")
    }
    
    private func filePathUrl()  {
        SplashVC.pathString = ""
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first ?? ""
        
        print("a1212")
        
        SplashVC.pathString = String(format: "%@/%@", path)
        
        
    }
}
