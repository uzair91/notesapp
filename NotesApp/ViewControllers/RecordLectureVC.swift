//
//  RecordLectureVC.swift
//  NotesApp
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit
import EZAudio
import CoreTelephony

protocol RecorderViewDelegate : class {
    func didFinishRecording(_ recorderViewController: RecordLectureVC)
}
class RecordLectureVC: UIViewController,RecorderDelegate ,UIPickerViewDelegate,UIPickerViewDataSource{//, AVAudioRecorderDelegate{
    @IBOutlet var mainView: UIView!
    
    @IBOutlet weak var pauseandresume: UIButton!
    @IBOutlet var pauseBtnandresume: UIView!
    let toolBar = UIToolbar()
    @IBOutlet weak var recordingImage: UIImageView!
    var categoryPicker: UIPickerView!
    var categoryPicker1: UIPickerView!
    open weak var delegate: RecorderViewDelegate?
    static var  recording: Recording!
    var recordDuration = 0
    @IBOutlet weak var txtDate: UILabel!
    
    public static var languagecode = "en-US"
    
    @IBOutlet weak var changelanguagae: UITextField!
    @IBOutlet weak var changeLanguage: UILabel!
    var GetAllDataInfo  = NSMutableArray()
    var categoryPickerValues = [String]()
    
    var categoryPickerValues1 = [String]()
    var pulseArray = [CAShapeLayer]()
    var starttime : String = ""
    var endtime : String = ""
    // var isPaused: Bool
    var urllastname : String?
    public static var image : UIImage?
    var image1 : UIImage?
    
    var counter : Int = 0
    
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var txtTime: UILabel!
    var timer: Timer?
    @IBOutlet weak var txtTitle: UITextField!
    var totalTime = 0//2700
    @IBOutlet weak var lblTimer: UILabel!
    //MARK:- My IBOutlets
//    @IBOutlet weak var plot: EZAudioPlotGL!
    @IBOutlet weak var navigationLbl: UILabel!
    @IBOutlet weak var recordBtn: UIButton!
    var isPaused = true
    //MARK:- My Helper Variables
    var microphone: EZMicrophone!
    var recorder : EZRecorder?
    
    var filepath : String = ""
    
    @IBOutlet weak var recordingView: UIView!
    
//    var recordingSession : AVAudioSession!
//    var audioRecorder    :AVAudioRecorder!
//    var settings         = [String : Int]()
    
    
    private var callCenter = CTCallCenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let currentDateTime = Date()
        
        
//        print("did load here")
        
         createRecorder()
        
        // get the user's calendar
        let userCalendar = Calendar.current
        
        // choose which date and time components are needed
        let requestedComponents: Set<Calendar.Component> = [
            .year,
            .month,
            .day,
            .hour,
            .minute,
            .second
        ]
        
        
         addToolBar(textField: txtTitle)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
     
        
     
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
       
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtCategory.inputAccessoryView = toolBar
        changelanguagae.inputAccessoryView = toolBar
        
        
        
        RecordLectureVC.image = #imageLiteral(resourceName: "record btn")
        
        image1 =  #imageLiteral(resourceName: "Group 20")
        
        NotificationCenter.default.addObserver(self, selector: #selector(doSomething), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)

       // detects call in swift
        
        callCenter.callEventHandler = { (call:CTCall!) in
            
            switch call.callState {
            case CTCallStateConnected:
//                println("CTCallStateConnected")
                self.callConnected()
            case CTCallStateDisconnected:
//                println("CTCallStateDisconnected")
                self.callDisconnected()
            default:
                //Not concerned with CTCallStateDialing or CTCallStateIncoming
                break
            }
        }
         categoryPickerValues1.append("Englsih")
        categoryPickerValues1.append("Spanish")
        

        
//        categoryPickerValues.append("Assorted")
        
//        txtCategory.text? = categoryPickerValues[0]
        
        
        
       GetAllDataInfo = FMDBDatabaseModel.getInstance().GetAllDataCateogry()
        
        
        for i in GetAllDataInfo
        {
            var l = CatgegoryModel()
            l = i as! CatgegoryModel
            
            print("cat name is here \(l.cat_name)")
           categoryPickerValues.append(l.cat_name)
            
        }
       
        
      
        
        let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: currentDateTime)


        self.txtDate.text = "\(String(describing: dateTimeComponents.day)) + \(String(describing: dateTimeComponents.month)) + \(String(describing: dateTimeComponents.year))"
        

        
        
        self.txtTime.text = "\(String(describing: dateTimeComponents.hour)) + \(String(describing: dateTimeComponents.minute))"
        
       
        // Do any additional setup after loading the view.
    
        setdateTime()
        setPicker()
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try session.setActive(true)
        }
        catch {
            debugPrint("Failed to initiate AVAudioSession")
            return
        }
        

        recordingImage.isUserInteractionEnabled = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RecordLectureVC.addPulse))
        tapGestureRecognizer.numberOfTapsRequired = 1
        recordingImage.addGestureRecognizer(tapGestureRecognizer)

      
        microphone = EZMicrophone(delegate: self, startsImmediately: false)
//        plot.backgroundColor = .white
//        plot?.plotType = EZPlotType.rolling
//        plot?.shouldFill = false
//        plot?.shouldMirror = false
    }
    
    func callConnected(){
        // Do something when call connects
        print("call connected")
    }
    
    func callDisconnected() {
        // Do something when call disconnects
        print("call disconnected")
    }
    
    @objc func appMovedToBackground() {
        print("App moved to background!")
        
//        if()
        
    }
    
    @objc private func doSomething() {
        
        
        if( RecordLectureVC.image == #imageLiteral(resourceName: "Group 394"))
        {
           createPulse(value: 0.9)
        }
        print("App moved to background111!")
        // Do whatever you want, for example update your view.
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        
       
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
            
          
            // When the user begins using the keyboard move the view containing the textfield
          
        }
        
        
    }
    
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
      
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func addPulse(){
//        let pulse = Pulsing(numberOfPulses: 1, radius: 110, position: CGPoint(x: recordingImage.frame.height/2, y: recordingImage.frame.width/2))
//
//
//        pulse.animationDuration = 0.8
//        pulse.backgroundColor = UIColor.blue.cgColor
//
//
//
//        self.view.layer.insertSublayer(pulse, below: recordingImage.layer)
        
     
        
    }
    
   @objc func doneClick() {
        view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        setdateTime()
        setPicker()
         counter  = 0
        
        categoryPickerValues.append("Assorted")
        
        txtCategory.text? = categoryPickerValues[0]
        
        changelanguagae.text? = categoryPickerValues1[0]
        
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try session.setActive(true)
        }
        catch {
            debugPrint("Failed to initiate AVAudioSession")
            return
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(callingScene), name: Notification.Name("calling"), object: nil)
        

        microphone = EZMicrophone(delegate: self, startsImmediately: false)
    }
    
    @objc func callingScene(notification: NSNotification) {
        
        
     
        print("yess calling scene here")
        
        if( RecordLectureVC.image != #imageLiteral(resourceName: "record btn"))
        {
            pauseRecordingFunctionality()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("hello pause here")
        
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("hello pause here111")
    }
    override func viewDidDisappear(_ animated: Bool) {
       print("hello pause here112")
    }
    
    func setPicker() {
    
        categoryPicker = UIPickerView()
        
        categoryPicker.dataSource = self
        categoryPicker.delegate = self
        
        categoryPicker1 = UIPickerView()
        
        categoryPicker1.dataSource = self
        categoryPicker1.delegate = self
        txtCategory.inputView=categoryPicker
            
            changelanguagae.inputView=categoryPicker1
        
       
        // txtType.text=vehiclePickerValues[0]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        if(txtCategory.isFirstResponder)
        {
            return categoryPickerValues.count
        }
        
        else
        {
             return categoryPickerValues1.count
        }
        
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(txtCategory.isFirstResponder)
        {
        return categoryPickerValues[row]
        }
        else
        {
            return categoryPickerValues1[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(txtCategory.isFirstResponder)
        {
            if(pickerView == categoryPicker)
            {
                txtCategory.text = categoryPickerValues[row]
                self.view.endEditing(true)
            }
        }
        else
        {
            if(pickerView == categoryPicker1)
            {
                if(categoryPickerValues1[row]=="en-US")
                {
                    RecordLectureVC.languagecode = "en-US"
                }
                else
                {
                    RecordLectureVC.languagecode = "es-419"
                }
               
                changelanguagae.text = categoryPickerValues1[row]
                self.view.endEditing(true)
            }
        }
       
        //        else
        //        {
        //        txtVehicle.text = vehiclePickerValues[row]
        //        self.view.endEditing(true)
        //        }
        
        
    }
    
   func setdateTime()
   {
        let dateFormatter : DateFormatter = DateFormatter()
        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.dateFormat = "dd MMM,yyyy HH:mma"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
    
    
    let fullNameArr = dateString.components(separatedBy: " ")
    
    let dateStr: String = fullNameArr[0] + fullNameArr[1]
    let time: String = fullNameArr[2]
    txtTime.text = time
    txtDate.text = dateStr
    
    }
    
    
    
    private func filePathUrl() -> URL {
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first ?? ""
        
        return URL(fileURLWithPath: String(format: "%@/%@", path, randomString(length: 10) + ".m4a"))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//     var isPaused:Bool
    //MARK:- My IBAction Methods
    @IBAction func recordBtnTapped(_ sender: Any) {
        
       
      
        
        if(RecordLectureVC.image == #imageLiteral(resourceName: "Group 394"))
        {
            var refreshAlert = UIAlertController(title: "Aleart", message: "Do you really want to stop recording?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
               
                self.stopRecordingFunction()
                
                
                print("Handle Ok logic here")
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(refreshAlert, animated: true, completion: nil)
          
        }
        
        else
        {
            if(counter==0)
            {
                if((txtTitle.text?.isEmpty)!)
                {
                    SharedHelper().showToast(message: "Please add title for lecture", controller: self)
                }
                else
                {
                    
                    if((txtCategory.text?.isEmpty)!)
                    {
                        SharedHelper().showToast(message: "Please add category of lecture", controller: self)
                    }
                    else
                    {
                        
                        
                        view.endEditing(true)
                        
                        let currentDateTime = Date()
                        
                        // get the user's calendar
                        let userCalendar = Calendar.current
                        
                        // choose which date and time components are needed
                        let requestedComponents: Set<Calendar.Component> = [
                            .year,
                            .month,
                            .day,
                            .hour,
                            .minute,
                            .second
                        ]
                        
                        // get the components
                        let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: currentDateTime)
                        
                        
                        //
                        //        self.txtDate.text = dateTimeComponents.day as? String
                        //            "+" dateTimeComponents.month! as? String "+" dateTimeComponents.year! as? String
                        
                        
                        
                        //        dateTimeComponents.day
                        
                        
                        
                        
                        
                        //                self.txtTime.text = "\(String(describing: dateTimeComponents.hour)) + \(String(describing: dateTimeComponents.minute))"
                        
                        
                        
                        
                        
                        self.starttime = "\(String(describing: dateTimeComponents.hour!)) + \(String(describing: dateTimeComponents.minute!)) + \(String(describing: dateTimeComponents.second!))"
                        
                        RecordLectureVC.image = recordBtn.currentImage == #imageLiteral(resourceName: "stopRedIcon") ? #imageLiteral(resourceName: "pauseIcon") : #imageLiteral(resourceName: "stopRedIcon")
                        
                        //                    image =  #imageLiteral(resourceName: "stopRedIcon")
                        //
                        //                    //                recordBtn.currentImage = image
                        //                    recordBtn.setImage(image, for: .normal)
                        RecordLectureVC.image =  #imageLiteral(resourceName: "Group 394")
                        
                        //                recordBtn.currentImage = image
                        recordBtn.setImage(RecordLectureVC.image, for: .normal)
                        //        navigationLbl.text = image == #imageLiteral(resourceName: "pauseIcon") ? "RECORDING LECTURE" : "START LECTURE"
                        
                        _ = RecordLectureVC.image == #imageLiteral(resourceName: "stopRedIcon") ? microphone.stopFetchingAudio() : microphone.startFetchingAudio()
                        
                        do
                        {
                            
                            
                            
                            counter = 1
                            createPulse(value: 0.9)
                            
                            try RecordLectureVC.recording.record()
                            self.timer = nil
                            //                        startRecording()
                        }
                        catch {
                            print("error is here\(error)")
                        }
                        
                        //                    isPaused = true
                        //                    startRecording()
                        
                        if(isPaused)
                        {
                            self.totalTime = 0
                            //                        isPaused = false
                            startOtpTimer()
                            
                            
                        }
                        else
                        {
                            //                        isPaused = true
                            //            timer!.invalidate()
                            self.timer = nil
                            startRecording()
                           
                            
                        }
                    }
                }
            }
            else
            {
                SharedHelper().showToast(message: "Press stop button to save", controller: self)
            }
            
            
        }
       
       
        
    }
    
    @IBAction func stopBtnTapped(_ sender: Any) {
       

        if( RecordLectureVC.image == #imageLiteral(resourceName: "record btn"))
        {
         SharedHelper().showToast(message: "Please record first", controller: self)
        }
        else
        {
            
            if (image1 ==  #imageLiteral(resourceName: "end"))
            {
                
                resumeRecordingFunction()
                
                image1 =  #imageLiteral(resourceName: "Group 20-1")

                pauseandresume.setImage(image1, for: .normal)
                
            
            }
            
            else
            {
                
                
              pauseRecordingFunctionality()
                
            }
            
        }

    }
    
    func pauseRecordingFunctionality()
    {
        stopPulse()
        
        pauseRecordingFunction()
        
        image1 =  #imageLiteral(resourceName: "end")
   
        
        pauseandresume.setImage(image1, for: .normal)
    }
    
    
    func resumeRecordingFunction()
    {
        createPulse(value: 0.9)
       startOtpTimer()
        microphone.startFetchingAudio()
        try! RecordLectureVC.recording.record()
    }
    
    func pauseRecordingFunction()
    {
        timer?.invalidate()
        microphone.stopFetchingAudio()
        RecordLectureVC.recording.pause()
    }
    
    func stopRecordingFunction()
    {
        let currentDateTime = Date()
        
        //        pulseArray[index].strokeColor = UIColor.clear.cgColor        // get the user's calendar
        let userCalendar = Calendar.current
        let requestedComponents: Set<Calendar.Component> = [
            .year,
            .month,
            .day,
            .hour,
            .minute,
            .second
        ]
        
        // get the components
        let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: currentDateTime)
        
        
        //        let pulsatingLayer = CAShapeLayer()
        ////        pulsatingLayer.path = circularPath.cgPath
        //        pulsatingLayer.lineWidth = 0
        //        pulsatingLayer.fillColor = UIColor.clear.cgColor
        //        pulsatingLayer.lineCap = kCALineCapRound
        //        pulsatingLayer.position = CGPoint(x: recordingImage.frame.size.width / 2.0, y: recordingImage.frame.size.width / 2.0)
        //        recordingImage.layer.addSublayer(pulsatingLayer)
        //        pulseArray.append(pulsatingLayer)
        
       
            if((txtTitle.text?.isEmpty)!)
            {
                SharedHelper().showToast(message: "Please add title for lecture", controller: self)
            }
            else
            {
                
                if((txtCategory.text?.isEmpty)!)
                {
                    SharedHelper().showToast(message: "Please add category of lecture", controller: self)
                }
                else
                {
                    self.endtime = "\(String(describing: dateTimeComponents.hour!)) + \(String(describing: dateTimeComponents.minute!)) + \(String(describing: dateTimeComponents.second!))"
                    RecordLectureVC.image =  #imageLiteral(resourceName: "record btn")
                    image1 =  #imageLiteral(resourceName: "Group 20")
                    pauseandresume.setImage(image1, for: .normal)
                    recordBtn.setImage(RecordLectureVC.image, for: .normal)
                    stop()
                }
            }
    }
    
    
  
    
    private func startOtpTimer() {
       // self.totalTime = 0
        
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        
    }
   
    
    @objc func updateTimer() {
        print(self.totalTime)
        self.lblTimer.text = self.timeFormatted(self.totalTime)
        // will show timer
        if totalTime == 2700 {
            if let timer = self.timer {
                timer.invalidate()
                self.timer = nil
            }
        } else {
            // decrease counter timer
             totalTime += 1
           
        }
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
  
 
    
    open func createRecorder() {
        
        urllastname = randomString(length: 10)
        RecordLectureVC.recording = Recording(to: urllastname!  + ".m4a")
        
        
        
       
        
//        recording=Bundle.main.path(forResource: "Sample.mp4", ofType:nil) as! String
        RecordLectureVC.recording.delegate = self
        
        filepath = RecordLectureVC.recording.getUrl().path

//         createRecorder()
//         NSData(contentsOfFile: filepath)!
        
        // Optionally, you can prepare the recording in the background to
        // make it start recording faster when you hit `record()`.
        
//        DispatchQueue.global().async {
//            // Background thread
//            do {
//                try self.recording.prepare()
//            } catch {
//                print(error)
//            }
//        }
    }
    
     func startRecording() {
        recordDuration = 0
        do {
            
            if(isPaused)
            {
                RecordLectureVC.recording.pause()
                
                
            }
            else
            {
                print("uzair here")
                
                try RecordLectureVC.recording.record()
            }
            
            
        } catch {
            print(error)
        }
    }
    
     func stop() {
        
        if(timer != nil)
        {
        timer!.invalidate()
        self.timer = nil
        }
       // startOtpTimer()
       // delegate?.didFinishRecording(self)
        //dismiss(animated: true, completion: nil)
        
        lblTimer.text = "00:00";
//        plot.clear()
        recordDuration = 0
        RecordLectureVC.recording.stop()
        
        print("recording url is here\(RecordLectureVC.recording.url)")
        do {
            
            print("urll of recording is here \(RecordLectureVC.recording.url)")
            
            let str = RecordLectureVC.recording.url;
            // let url = URL(fileURLWithPath: str)
            
            print("string is here \(str)")
            
            let audioData = try Data(contentsOf: str, options: NSData.ReadingOptions())
            
           
//            let audioData1 = try NSData(contentsOf: str, options: NSData.ReadingOptions())
//            print(audioData)
//             print(audioData1)
            self.sendAudio(audioData1: audioData)
            
            
        } catch {
            print(error)
        }
//        voiceRecordHUD.update(0.0)
        
    }
    
    func saveRecordingOffline()
    {
         SharedHelper().showSpinner(view: self.view)
       
        
         let message = "Saved locally"
        SharedHelper().showToast(message: message, controller: self)
        
        let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: MainStoryBoard.LectureDetails.rawValue) as! LectureDetailsVC
        
        vc.pid = ""
        
        //
        //                        lebLtime1.text = strTime
        //                        lbldateTime.text = strDateTime
        
        
        vc.strCat = self.txtTitle.text!
        vc.strDateTime = self.txtDate.text!
        
        vc.filepath = self.filepath
        
        
        
        vc.lectureModel = self.saveRecordDb(pid: "")
        
        vc.strTime = (vc.lectureModel?.getlength())! + " seconds"
        vc.duration = (vc.lectureModel?.getlength())!
        self.txtTitle.text = ""
        self.txtCategory.text = ""
        
        
         SharedHelper().hideSpinner(view: self.view)
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    func saveRecordingWheninternetConnection(audioData1 : Data)
    {
        
        SharedHelper().showSpinner(view: self.view)
         let url = APP_CONSTANT.BASE_URL + APP_CONSTANT.UPLOAD
        
        var fcmToken = ""
        
        if let t =  UserDefaults.standard.string(forKey: "FCMToken") as String?
        {
            fcmToken = t
        }
        
        print("fcmstring\(fcmToken)")
        print(fcmToken)
        let param = [
            "user_id": "200",
            "audio_id": "200",
            "device_type": "ios",
            "token": fcmToken ,
            "language" : RecordLectureVC.languagecode
        ]
        
        SharedHelper().addAudioFile(url: url, audioData: audioData1, parameters: param)    {
            response in
            print("here is response : \(response)")
            if response.result.value == nil {
                print("No response")
                SharedHelper().hideSpinner(view: self.view)
                return
            }
            else {
                let responseData = response.result.value as! NSDictionary
                SharedHelper().hideSpinner(view: self.view)
                let message = responseData["response"] as! String
                let PID = responseData["PID"] as! Int
                
                
                if(message == "Uploaded Successfully")
                {
                    //                        print("yes yes yes yes yes yes")
                    
                    SharedHelper().hideSpinner(view: self.view)
                    SharedHelper().showToast(message: message, controller: self)
                    
                    let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: MainStoryBoard.LectureDetails.rawValue) as! LectureDetailsVC
                    
                    vc.pid = String(PID)
                    
                    //
                    //                        lebLtime1.text = strTime
                    //                        lbldateTime.text = strDateTime
                    
                    
                    vc.strCat = self.txtTitle.text!
                    vc.strDateTime = self.txtDate.text!
                    
                    vc.filepath = self.filepath
                    
                    
                    
                    vc.lectureModel = self.saveRecordDb(pid: String(PID))
                    
                    vc.strTime = (vc.lectureModel?.getlength())! + " seconds"
                    vc.duration = (vc.lectureModel?.getlength())!
                    self.txtTitle.text = ""
                    self.txtCategory.text = ""
                    
                    
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    
                    
                    
                    //                    let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: MainStoryBoard.References.rawValue) as! LectureDetailsVC
                    //
                    //                    self.navigationController?.pushViewController(vc, animated: true)
                    //
                    
                    
                }
                
                
            }
        }
    }
    
    func sendAudio(audioData1 : Data) {
        
        
        
        
       
        
        
        if(SharedHelper().isInternetAvailable())
        {
//            user_id:1
//            audio_id:1
//            device_type:ios
//            token:fdgafdgfdh
            
            
            
            
            
            //            let param = [
            //                "token": fcmToken
            //            ]
          
            
           
            
            saveRecordingWheninternetConnection(audioData1: audioData1)
          
            
        }
        else
        {
            
            saveRecordingOffline()
//            SharedHelper().showToast(message: "Check your internet connection", controller: self)
        }
//        let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: "LectureDetailsVC") as! LectureDetailsVC
//        vc.strCat = txtCategory.text
//        vc.strTitle = txtTitle.text
//        vc.strDateTime = (txtDate.text as! String) + (txtTime.text as! String)
//        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func saveRecordDb(pid : String) -> LectureModel
    {
       

                var lectureModel = LectureModel()
                lectureModel.settitle(title:self.txtTitle.text! )
                lectureModel.setcategory(category: self.txtCategory.text!)
                lectureModel.setdate(date:self.txtDate.text! )
                lectureModel.setdecsricption(decsricption:"" )
                lectureModel.setbookmark(bookmark: "0")
                lectureModel.setname(name:"" )
                lectureModel.setfilepath(filepath: self.urllastname!+".m4a")
                lectureModel.setlength(length: String(self.totalTime))
                lectureModel.settimestart(timestart: self.starttime)
                lectureModel.settimeend(timeend: self.endtime)
                lectureModel.setprocessID(processID:pid )
        
        if(RecordLectureVC.languagecode == "es-419")
        {
          lectureModel.setisDownaloded(isDownaloded:1)
        }
        else
        {
            
            lectureModel.setisDownaloded(isDownaloded:0)
        
        }
        
        
        
        
        
        
//        lectureModel.setisConverted(isConverted: 0)
//        lectureModel.setlang(lang: RecordLectureVC.languagecode)
        
        
        //
                FMDBDatabaseModel.getInstance().InsertData(lectureModel)
        
        return lectureModel
//        recording.stop()
        //        FMDBDatabaseModel.getInstance().getData()

    }
    
    func stopPulse() {
        
       createPulse(value: 0.0)
        
    }
    
    func createPulse(value : Double) {
        
        for _ in 0...2 {
            let circularPath = UIBezierPath(arcCenter: .zero, radius: ((self.recordingImage.superview?.frame.size.width )! )/2, startAngle: 0, endAngle: 2 * .pi , clockwise: true)
            let pulsatingLayer = CAShapeLayer()
            pulsatingLayer.path = circularPath.cgPath
            pulsatingLayer.lineWidth = 70
            pulsatingLayer.fillColor = UIColor.clear.cgColor
            pulsatingLayer.lineCap = kCALineCapRound
            pulsatingLayer.position = CGPoint(x: recordingImage.frame.size.width / 2.0, y: recordingImage.frame.size.width / 2.0)
            recordingImage.layer.addSublayer(pulsatingLayer)
            pulseArray.append(pulsatingLayer)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.animatePulsatingLayerAt(index: 0, value: value)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                
                self.animatePulsatingLayerAt(index: 1, value: value)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    
                    self.animatePulsatingLayerAt(index: 2, value: value)
                })
            })
        })
    
    }
    
    func animatePulsatingLayerAt(index:Int,value : Double) {
        
        //Giving color to the layer
        pulseArray[index].strokeColor = UIColor.white.cgColor
        
        //Creating scale animation for the layer, from and to value should be in range of 0.0 to 1.0
        // 0.0 = minimum
        //1.0 = maximum
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = 0.0
        scaleAnimation.toValue = value
        
        //Creating opacity animation for the layer, from and to value should be in range of 0.0 to 1.0
        // 0.0 = minimum
        //1.0 = maximum
        let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        opacityAnimation.fromValue = value
        opacityAnimation.toValue = 0.0
        
        // Grouping both animations and giving animation duration, animation repat count
        let groupAnimation = CAAnimationGroup()
        groupAnimation.animations = [scaleAnimation, opacityAnimation]
        groupAnimation.duration = 2.3
        groupAnimation.repeatCount = .greatestFiniteMagnitude
        groupAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        //adding groupanimation to the layer
        pulseArray[index].add(groupAnimation, forKey: "groupanimation")
        
    }
    
    func animatePulsatingLayerAt1(index:Int) {
        
        //Giving color to the layer
        pulseArray[index].strokeColor = UIColor.white.cgColor
        
        //Creating scale animation for the layer, from and to value should be in range of 0.0 to 1.0
        // 0.0 = minimum
        //1.0 = maximum
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = 0.0
        scaleAnimation.toValue = 0.0
        
        //Creating opacity animation for the layer, from and to value should be in range of 0.0 to 1.0
        // 0.0 = minimum
        //1.0 = maximum
        let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        opacityAnimation.fromValue = 0.0
        opacityAnimation.toValue = 0.0
        
        // Grouping both animations and giving animation duration, animation repat count
        let groupAnimation = CAAnimationGroup()
        groupAnimation.animations = [scaleAnimation, opacityAnimation]
        groupAnimation.duration = 2.3
        groupAnimation.repeatCount = .greatestFiniteMagnitude
        groupAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        //adding groupanimation to the layer
        pulseArray[index].add(groupAnimation, forKey: "groupanimation")
        
    }
    
    func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }

//    @IBAction func start() {
//       // self.present(recorderView, animated: true, completion: nil)
//        recorderView.startRecording()
//    }
//
//    @IBAction func play() {
//        do {
//            try recorderView.recording.play()
//            print(recorderView.recording)
//        } catch {
//            print(error)
//        }
//    }
    
//    internal func didFinishRecording(_ recorderViewController: RecorderViewController) {
//        print(recording.url)
//        do {
//            let str = recording.url;
//            // let url = URL(fileURLWithPath: str)
//            let weatherData = try NSData(contentsOf: str, options: NSData.ReadingOptions())
//            print(weatherData)
//        } catch {
//            print(error)
//        }
//
//    }
}


extension RecordLectureVC : EZMicrophoneDelegate {
    func microphone(_ microphone: EZMicrophone!, hasAudioReceived buffer: UnsafeMutablePointer<UnsafeMutablePointer<Float>?>!, withBufferSize bufferSize: UInt32, withNumberOfChannels numberOfChannels: UInt32) {
        
        DispatchQueue.main.async(execute: { () -> Void in
//            self.plot?.updateBuffer(buffer[0], withBufferSize: bufferSize)
            
        })
    }
  
//    mi
}

extension UIImageView {
    public func maskCircle(anyImage: UIImage) {
        self.contentMode = UIViewContentMode.scaleAspectFill
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = false
        self.clipsToBounds = true
        
        // make square(* must to make circle),
        // resize(reduce the kilobyte) and
        // fix rotation.
        self.image = anyImage
    }
}

extension UIView {
    
    func addBottomRoundedEdge(desiredCurve: CGFloat?,borderColor: CGColor) {
        let offset: CGFloat = self.frame.width / desiredCurve!
        let bounds: CGRect = self.bounds
        
        let rectBounds: CGRect = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height / 2)
        let rectPath: UIBezierPath = UIBezierPath(rect: rectBounds)
        let ovalBounds: CGRect = CGRect(x: bounds.origin.x - offset / 2, y: bounds.origin.y, width: bounds.size.width + offset, height: bounds.size.height)
        let ovalPath: UIBezierPath = UIBezierPath(ovalIn: ovalBounds)
        rectPath.append(ovalPath)
        
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = rectPath.cgPath
        maskLayer.shadowColor = borderColor
        maskLayer.shadowOpacity = 10.0
        
        self.layer.mask = maskLayer
        self.layer.borderWidth = 0
        self.layer.borderColor = borderColor
    }
}
