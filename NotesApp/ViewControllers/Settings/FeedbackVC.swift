//
//  FeedbackVC.swift
//  NotesAppHelperProject
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit

class FeedbackVC: UIViewController {

    @IBOutlet weak var suggestionsBtnActive: UIButton!
    @IBOutlet weak var suggestionsBtnInactive: UIButton!
    
    @IBOutlet weak var questionBtnActive: UIButton!
    @IBOutlet weak var questionBtnInactive: UIButton!
    
    @IBOutlet weak var problemBtnActive: UIButton!
    @IBOutlet weak var problemBtnInactive: UIButton!
    
    @IBOutlet weak var praiseBtnActive: UIButton!
    @IBOutlet weak var praiseBtnInactive: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func suggestionsTapped(_ sender: UIButton) {
        buttonActiveChange(active: suggestionsBtnActive, inActive: suggestionsBtnInactive, isActiveState: sender.tag == 1 ? true:false)
    }
    
    @IBAction func questionTapped(_ sender: UIButton) {
        buttonActiveChange(active: questionBtnActive, inActive: questionBtnInactive, isActiveState: sender.tag == 1 ? true:false)
    }
    
    @IBAction func problemTapped(_ sender: UIButton) {
        buttonActiveChange(active: problemBtnActive, inActive: problemBtnInactive, isActiveState: sender.tag == 1 ? true:false)
    }
    
    @IBAction func praiseTapped(_ sender: UIButton) {
        buttonActiveChange(active: praiseBtnActive, inActive: praiseBtnInactive, isActiveState: sender.tag == 1 ? true:false)
    }
    
    func buttonActiveChange(active:UIButton, inActive:UIButton, isActiveState:Bool){
        
        if isActiveState{
            active.setImage(UIImage.init(named:"button-1"), for: .normal)
            inActive.setImage(UIImage.init(named:"button-2"), for: .normal)
        }else{
        
        active.setImage(UIImage.init(named:"button-4"), for: .normal)
            
        inActive.setImage(UIImage.init(named:"button-3"), for: .normal)
        }
    }
    
}
