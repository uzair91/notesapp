//
//  NotificationsVC.swift
//  NotesAppHelperProject
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit

class NotificationsVC: UIViewController {

    @IBOutlet weak var generalUpdatesActive: UIButton!
    
    @IBOutlet weak var generalUpdatesinActive: UIButton!
    
    @IBOutlet weak var otherUpdateActive: UIButton!
    @IBOutlet weak var otherUpdateInactive: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func generalUpdatesTapped(_ sender: UIButton) {
        buttonActiveChange(active: generalUpdatesActive, inActive: generalUpdatesinActive, isActiveState: sender.tag == 1 ? true:false)
    }
    
    @IBAction func otherUpdatesTapped(_ sender: UIButton) {
        buttonActiveChange(active: otherUpdateActive, inActive: otherUpdateInactive, isActiveState: sender.tag == 1 ? true:false)
    }
    
    func buttonActiveChange(active:UIButton, inActive:UIButton, isActiveState:Bool){
        
        if isActiveState{
            active.setImage(UIImage.init(named:"button-1"), for: .normal)
            inActive.setImage(UIImage.init(named:"button-2"), for: .normal)
        }else{
            
            active.setImage(UIImage.init(named:"button-4"), for: .normal)
            
            inActive.setImage(UIImage.init(named:"button-3"), for: .normal)
        }
    }
    
}
