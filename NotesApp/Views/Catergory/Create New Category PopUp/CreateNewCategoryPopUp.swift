//
//  CreateNewCategoryPopUp.swift
//  NotesAppHelperProject
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit
import KUIPopupController

class CreateNewCategoryPopUp: UIView , NibLoadableView, KUIPopupContentViewProtocol {
    var animator: KUIPopupContentViewAnimator?
    var yourobj : (() -> Void)? = nil
    
    class func instanceFromNib() -> CreateNewCategoryPopUp? {
        let view = Bundle.main.loadNibNamed(CreateNewCategoryPopUp.NibName, owner: nil, options: nil)?.first as? CreateNewCategoryPopUp
        
        
       
        
       
        
        return view
    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        dismiss(true)
    }
    
    
    @IBOutlet weak var categoryTitle: UITextField!
    
    @IBAction func savebTN(_ sender: Any) {
        
        
        if(categoryTitle.text=="")
        {
//            SharedHelper().showToast(message: "Please fill category field", controller: self)
            dismiss(true)
        }
        else
        {
            FMDBDatabaseModel.getInstance().InsertDataCategory(cat_name: categoryTitle.text!)
       
            if let btnAction = self.yourobj
                    {
                        btnAction()
                        //  user!("pass string")
                    }
             dismiss(true)
        }
        
       
        
//        if let btnAction = self.yourobj
//        {
//            btnAction()
//            //  user!("pass string")
//        }
    }
    
    
}
