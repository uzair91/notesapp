//
//  EditCategoryPopup.swift
//  NotesApp
//
//  Created by Basit on 5/1/19.
//  Copyright © 2019 APP. All rights reserved.
//

import UIKit
import KUIPopupController
class EditCategoryPopup: UIView , NibLoadableView, KUIPopupContentViewProtocol {
    var animator: KUIPopupContentViewAnimator?
var yourobj : (() -> Void)? = nil
    
    class func instanceFromNib() -> EditCategoryPopup? {
        let view = Bundle.main.loadNibNamed(EditCategoryPopup.NibName, owner: nil, options: nil)?.first as? EditCategoryPopup
        
        return view
    }
    
    @IBOutlet weak var cateogryText: UITextField!
    
    @IBAction func saveBtn(_ sender: Any) {
        
        if(cateogryText.text=="")
        {
            //            SharedHelper().showToast(message: "Please fill category field", controller: self)
            dismiss(true)
        }
        else
        {
            FMDBDatabaseModel.getInstance().InsertDataCategory(cat_name: cateogryText.text!)
            
            if let btnAction = self.yourobj
            {
                btnAction()
                //  user!("pass string")
            }
            dismiss(true)
        }
        
    }
    @IBAction func cancelBtn(_ sender: Any) {
        
        dismiss(true)
    }
    /*
     @IBAction func cancelBtn(_ sender: Any) {
     }
     @IBOutlet weak var saveBtn: UIButton!
     // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
