//
//  EditCateogryPopup.swift
//  NotesApp
//
//  Created by Basit on 5/1/19.
//  Copyright © 2019 APP. All rights reserved.
//

import UIKit
import KUIPopupController


protocol DismissDelegate {
    func getText(title : String)->String
}

class EditCateogryPopup: UIView , NibLoadableView, KUIPopupContentViewProtocol {
    var animator: KUIPopupContentViewAnimator?
    var yourobj : (() -> Void)? = nil
    
    
    var id : Int?
    var title : String?
    

    class func instanceFromNib() -> EditCateogryPopup? {
        let view = Bundle.main.loadNibNamed(EditCateogryPopup.NibName, owner: nil, options: nil)?.first as? EditCateogryPopup
        
        
        
        return view
    }
    
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        dismiss(true)
    }
    
    
    @IBOutlet weak var categoryTitle: UITextField!
    
   
    
    @IBAction func savebTN(_ sender: Any) {
        
        
//        print("title is here \(title!)")
        
        if(categoryTitle.text=="")
        {
            //            SharedHelper().showToast(message: "Please fill category field", controller: self)
            dismiss(true)
        }
        else
        {
            
//            FMDBDatabaseModel.getInstance().InsertDataCategory(cat_name: categoryTitle.text!)
            
//            FMDBDatabaseModel.getInstance().updateCategory(descritpion: categoryTitle.text!, id: <#T##Int#>)
//
            FMDBDatabaseModel.getInstance().updateCategory(descritpion: categoryTitle.text!, id: self.id!)
            if let btnAction = self.yourobj
            {
                btnAction()
                //  user!("pass string")
            }
            dismiss(true)
        }
        
        
        
        //        if let btnAction = self.yourobj
        //        {
        //            btnAction()
        //            //  user!("pass string")
        //        }
    }
    
    
}


