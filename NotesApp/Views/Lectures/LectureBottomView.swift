//
//  LectureBottomView.swift
//  NotesAppHelperProject
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit
import FSPagerView


class LecturesBottomSliderView: FSPagerViewCell, NibLoadableView, ReusableView {

    @IBOutlet weak var itemNameLbl: UILabel!
}
