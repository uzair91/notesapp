//
//  LeftMenuTVC.swift
//  
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit

class LeftMenuTVC: UITableViewCell, NibLoadableView, ReusableView {

    @IBOutlet weak var itemLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var itemImage: UIImageView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
