//
//  ReferencesDetailVC.swift
//  NotesApp

//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit

class ReferencesDetailVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var strTitle : String!
    var bookCode : String = ""
    var bookiD : String = ""
    var pid : String = ""
    var resultArr:[Any] = []
    var bookkey : String = ""
    
    var bookname = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        
        tableView.register(UINib(nibName: ReferencesTVC.NibName , bundle: nil), forCellReuseIdentifier: ReferencesTVC.reuseIdentifier)
        
        self.tableView.separatorStyle = .none
//        self.tableView.estimatedRowHeight = 88.0
//        self.tableView.rowHeight = UITableViewAutomaticDimension
        getRefrence()
    }
    
    
    func getRefrence() {
        //        let fcmToken = UserDefaults.standard.string(forKey: "FCMToken")
        //        print(fcmToken)
        SharedHelper().showSpinner(view: self.view)
       
//        let url = APP_CONSTANT.GET_SEARCH_BY_KEYWORD + strTitle
        
        
        
        
        
        if(FMDBDatabaseModel.getInstance().getLanguageCode(pid: pid) == 0)
        {
            bookkey = "179568874c45066f-01"
            bookname = "Douay-Rheims American 1899"
            bookCode = "478"
            bookiD = "JHN.1.darby"
        }
        else
        {
            
            
           
            bookkey = "592420522e16049f-01"
            bookname = "Reina Valera Bible 1909"
            bookCode = "147"
            bookiD = "JHN.1.rves"
            
            
        }
        
        
       
        
        let url = "https://api.scripture.api.bible/v1/bibles/"+bookkey+"/search?query="+strTitle+"&limit=200"
        
        
        
        
        SharedHelper().GetRequest(url: url)    {
            response in
            print("REFERENCE RESPONSE\(response)")
            if response.result.value == nil {
                print("No response")
                SharedHelper().hideSpinner(view: self.view)
                SharedHelper().showToast(message: "No response", controller: self)
                
//                SharedHelper().hideSpinner(view: self.view)
                return
            }
            else {
//                JSON(data: response.data!)
                
//                let responseData = response.result.value as! NSDictionary
////                let responseData = response.result.value as! NSDictionary
//
//                let responseData1 = responseData["data"] as! NSDictionary
                
               
                let responseData = response.result.value as! NSDictionary
                let responseData1 = responseData["data"] as! NSDictionary
                
                
//                print("response is here \(response)")
                
                let count = responseData1["total"] as! NSNumber
                print("count is here \(count)")
//
                if(count==0)
                {
                     SharedHelper().hideSpinner(view: self.view)
                     SharedHelper().showToast(message: "Your search produced no results", controller: self)
                }
                else
                {
                     SharedHelper().hideSpinner(view: self.view)
                    self.resultArr = (responseData1["verses"] as! [Any] as NSArray) as! [Any]
                      self.tableView.reloadData()
                }
                

//                self.resultArr = (responseData1["results"] as! [Any] as NSArray) as! [Any]

              
//
//
//
//                if(status==4)
//                {
////                    let status = responseData["error_level"] as! NSNumber
////                    self.resultArr = (responseData["results"] as! [Any] as NSArray) as! [Any]
//                    SharedHelper().hideSpinner(view: self.view)
//                    SharedHelper().showToast(message: "Your search produced no results", controller: self)
//
////                    SharedHelper().hideSpinner(view: self.view)
////                    SharedHelper().showToast(message: "No response", controller: self)
//                    self.tableView .reloadData()
//                    SharedHelper().hideSpinner(view: self.view)
//
//                }
//                else
//                {
//
//                     SharedHelper().hideSpinner(view: self.view)
////                    let message = responseData["results"] as! [[String:NSDictionary]]
//
//
////                    let message1 = message[1] as! NSDictionary
////                    print("message is here \(message.value(forKey: "book_name"))")
////                    print("meesage is here  \(String(describing: message[0]["verses"]["kjv"]["4"]["3"]["text"]))")
////                    SharedHelper().showToast(message: message[1] as! String, controller: self)
////                    SharedHelper().hideSpinner(view: self.view)
//                }
                
                // print(responseData)
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getText(dict : NSDictionary) -> String {
        
        
        let strChapter = dict["chapter_verse"] as! String
        let arrChapter = strChapter.components(separatedBy: ":")
        
        let first: String = arrChapter[0]
        let second: String = arrChapter[1]
        
             let verses = dict["verses"] as! NSDictionary
        let versesmain = verses["kjv"] as! NSDictionary
        let versesFirst = versesmain[first] as! NSDictionary
        let versesSecond = versesFirst[second] as! NSDictionary
        
        let strText = versesSecond["text"] as! String
        
        
        
        return strText;
    }
}

extension ReferencesDetailVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.resultArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReferencesTVC.reuseIdentifier, for: indexPath) as! ReferencesTVC
        cell.selectionStyle = .none
        if(self.resultArr.count>0)
        {
            
//            "book_id": 4,
//            "book_name": "Numbers",
//            "book_short": "Num",
//            "book_raw": null,
//            "chapter_verse": "12:7",
//            "chapter_verse_raw": "12:7",
//            "verse_index": {
//                "12": [
//                7
//                ]
//            },
//            "verses": {
//                "kjv": {
//                    "12": {
//                        "7": {
//                            "id": 4067,
//                            "book": 4,
//                            "chapter": 12,
//                            "verse": 7,
//                            "text": "My servant Moses is not so, who is faithful in all mine house.",
//                            "italics": ""
//                        }
//                    }
//                }
//            },
//            "verses_count": 1,
//            "single_verse": true,
//            "nav": []
            
           
            
            
           
                    //Do whatever you want to do when the button is tapped here
            
           
                    let dict = self.resultArr[indexPath.row] as! NSDictionary
                    
//                    let strHeader = dict["book_name"] as! String
            
           
            let strHeader =  bookname
//                    let strSubHeader = (dict["book_name"] as! String) + " " + (dict["chapter_verse"] as! String)
//                    let strText = self.getText(dict: dict)
//                    let book_id = dict["book_id"] as! NSInteger
            
            let strSubHeader = bookname + " " + (dict["chapterId"] as! String)
            let strText = dict["text"] as! String
            let book_id = dict["bookId"] as! String
            
                    cell.lblHeader.text = strHeader
                    cell.lblSubHeader.text = strSubHeader
                    cell.lblDescription.text = strText
//                    cell.tag = book_id
            

        }
        
        let dict = self.resultArr[indexPath.row] as! NSDictionary
        
//        let strId = dict["book_name"] as! String
        
        let strId = bookname
        
      
        
        let strUrl = "https://www.bible.com/en-GB/bible/"+bookCode+"/"+bookiD
        
        print("uzair reference here")
        cell.yourobj =
            {
                if let url = NSURL(string: strUrl){
//                    UIApplication.shared.openURL(url as URL)
                    
                    
                   
                    
                    let vc = UIStoryboard.mainStoryboard().instantiateViewController(withIdentifier: "ReferenceFullTextController") as! ReferenceFullTextController
                    
                   
                    
                    vc.url = url as URL
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
        }
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//         let dict = self.resultArr[indexPath.row] as! NSDictionary
//
//        let strId = dict["book_name"] as! String
//        let strUrl = "https://www.bible.com/en-GB/bible/1/" + strId
//
//         print("uzair reference here")
//        cell.yourobj =
//            {
//        if let url = NSURL(string: strUrl){
//            UIApplication.shared.openURL(url as URL)
//        }
//        }
//
//    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        // Swift 4.1 and below
//        return UITableViewAutomaticDimension
//    }
}

