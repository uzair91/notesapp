//
//  ReferencesTVC.swift
//  NotesApp
//
//  Created by Waqar Jamsheed on 07/10/2018.
//  Copyright © 2018 Waqar Jamsheed. All rights reserved.
//

import UIKit

class ReferencesTVC: UITableViewCell, NibLoadableView, ReusableView {

    
    var yourobj : (() -> Void)? = nil
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblSubHeader: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func getReferences(_ sender: Any) {
        
        if let btnAction = self.yourobj
        {
            btnAction()
            //  user!("pass string")
        }
        
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        
        // Configure the view for the selected state
    }
    
}
